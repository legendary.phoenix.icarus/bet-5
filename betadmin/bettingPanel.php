<?php
include './header.php';
include './side.php';
include 'db.php';
if (isset($_COOKIE['adminType'])) {
    $adminType = $_COOKIE['adminType'];
} else {
    $adminType = $_SESSION['adminType'];
}

date_default_timezone_set('Asia/Dhaka'); 

if (isset($_COOKIE['adminPanel'])) {

    $adminId = $_COOKIE['adminId'];
} else {

    $adminId = $_SESSION['adminId'];
}
?>
<link rel="stylesheet" type="text/css" href="css/betPanel.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">

<!-- Bootstrap Material Datetimepicker Plugin -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/> -->

<link rel="stylesheet" href="./css/bootstrap-material-datetimepicker.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<main class="app-content">
    <div class="app-title">
        <div>
            <h6><i class="fa fa-dashboard"></i>Total User Balance= 

                <?php
                    $query = "select sum(balance) as total from user";
                    $UserTotalBalance = $db->select($query);
                    if ($UserTotalBalance) {
                        $UserTotalBalance = $UserTotalBalance->fetch_assoc();
                        echo round($UserTotalBalance['total'], 2);
                    }

                    // $query = "SELECT * FROM `bettintransaction`";
                    // $query = "SELECT SUM(totalSending) as totalGaining, SUM(totalGaining) as totalSending, SUM(totalSaving) as totalSaving, SUM(s_save) as s_save FROM `bettintransaction`";

                    // $amount = $db->select($query);
                    // $totalGain = '';
                    // $totalSending = '';
                    // $totalSaving = '';
                    // $s_save = '';
                    // if ($amount){
                        // $total = $amount->fetch_assoc();
                        // $totalGain = $total['totalGaining'];
                        // $totalSending = $total['totalSending'];
                        // $totalSaving = $total['totalSaving'];
                        // $s_save = $total['s_save'];
                    // }
                                    
                                    $query = "select sum(amount) as total from deposit_and_withdraw_his where d_or_w = 1";
                    $UserTotalBalance = $db->select($query);
                    if ($UserTotalBalance) {
                        $UserTotalBalance = $UserTotalBalance->fetch_assoc();
                        $totalDeposit = round($UserTotalBalance['total'],2);
                    }

                    $query = "select sum(amount) as total from deposit_and_withdraw_his where d_or_w = 2";
                    $UserTotalBalance = $db->select($query);
                    if ($UserTotalBalance) {
                        $UserTotalBalance = $UserTotalBalance->fetch_assoc();
                        $totalWithdrawal = round($UserTotalBalance['total'],2);
                    }

                    // get win_save_amount
                    $query = "SELECT SUM(returnAmount) as total_return_amount, SUM(betAmount) as total_return_bet_amount FROM bet WHERE betStatus=1";
                    $total_win_bet = $db->select($query);
                    if ($total_win_bet) {
                        $total_win_bet = $total_win_bet->fetch_assoc();
                        $total_win_save_amount = round($total_win_bet['total_return_amount'] - $total_win_bet['total_return_bet_amount'],2);
                    }


                    // get rose_save_amount
                    $query = "SELECT SUM(betAmount) as total_lose_amount FROM bet WHERE betStatus=2";
                    $total_lose_query = $db->select($query);
                    if ($total_lose_query) {
                        $total_lose_query = $total_lose_query->fetch_assoc();
                        $total_lose_amount = round($total_lose_query['total_lose_amount'],2);
                    }

                    $total_save_amount = $total_lose_amount - $total_win_save_amount;

                    $query = "SELECT SUM((bet.betAmount * user.sponsorCommission/100)) AS total_s_amount FROM bet 
                                            LEFT JOIN USER ON(bet.userId = user.userId) WHERE bet.betStatus=1";
                    $total_s_amount_sql = $db->select($query);

                    if ($total_s_amount_sql) {
                        $total_s_amount_sql = $total_s_amount_sql->fetch_assoc();
                        $total_s_amount = round($total_s_amount_sql['total_s_amount'],2);
                    }
                ?>

                || Total Gaining Amount= <?php echo round($totalDeposit - $totalWithdrawal, 2); ?>
                || Total Sending Amount= <?php echo round($totalWithdrawal, 2); ?>
                || Total Saving Amount= <?php echo round($total_save_amount, 2); ?>
                <span style="color: green">  || Total S Amount = <?php echo round($total_s_amount, 2); ?> </span>
            </h6>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">



            <div class="tile">
                <div class="tile-body">
                    <!-- add match -->
                    <div id="add-bet" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Match Title </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">



                                    <div id="addMatchSuccess"></div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">A Team</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="A_team" name="A_team" rows="4" placeholder="Enter A Team Name">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">B Team</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="B_team" name="B_team" rows="4" placeholder="Enter  B Team Name">
                                             
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Bet Statement</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="title" name="title" rows="4" placeholder="Enter Bet Statement">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="date" name="date" value="<?php echo date("d M h:i A"); ?>" type="text" placeholder="">
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        <label class="control-label col-md-3">Time</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="time" name="time" value="" type="text" placeholder="">
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Match Status</label>
                                        <div class="col-md-8">
                                            <!--Radio Button Markup-->
                                            <div class="">
                                                <label>
                                                    <input type="radio" name="status" id="status" value="1"><span class="label-text"> Live</span><br>
                                                    <input type="radio" name="status" id="status" value="2"><span class="label-text"> Upcoming</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Match Type</label>
                                        <div class="col-md-8">
                                            <!--Radio Button Markup-->
                                            <div class="">
                                                <label>
                                                    <input type="radio" name="gameType" id="gameType" value="1"><span class="label-text"> FootBall</span><br>
                                                    <input class="gameTypec" type="radio" name="gameType" id="gameType" value="2"><span class="label-text"> Cricket</span><br>
                                                    <input type="radio" name="gameType" id="gameType" value="3"><span class="label-text"> Basketball</span><br>

                                                          <input type="radio" name="gameType" id="gameType" value="6"><span class="label-text"> Table Tennis </span>
                                                          <br>
                                                          <input type="radio" name="gameType" id="gameType" value="7"><span class="label-text"> Badminton</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row hiddenOp" style="display: none">
                                        <label class="control-label col-md-3">Match Status</label>
                                        <div class="col-md-8">
                                            <!--Radio Button Markup-->
                                            <div class="">
                                                <label>
                                                    <input type="radio" name="status2" id="status2" value="1"><span class="label-text"> ODI</span><br>
                                                    <input type="radio" name="status2" id="status2" value="2"><span class="label-text"> T20</span><br>
                                                    <input type="radio" name="status2" id="status2" value="3"><span class="label-text"> Test</span>
                                                
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input  style="background-color: #996600; border: 1px solid #996600;" id="addMatchSubmit" type="submit" class="btn btn-success" value="submit">
                                            <input  style="background-color: #996600; border: 1px solid #996600;"  id="addMatchSubmitDefault" type="submit" class="btn btn-success" value="add default">
                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- hidden-match -->
                    <div id="hidden-match" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Hidden match</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body" id="hiddenContentShow">

                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->

                    <a id="new" class="btn btn-primary icon-btn" href="" data-toggle="modal" data-target="#add-bet"><i class="fa fa-plus"></i>Add Item	</a>
                    <a id="new" class="btn btn-success" href="" ><i class="fa fa-refresh"></i>Refresh</a>
                    <a id="new" class="btn btn-primary" href="" id="hidden" data-toggle="modal" data-target="#hidden-match">hidden match </a>
                    <a id="new" class="btn btn-primary" href="defaultSetting.php" >Set default match </a><br><br>
                    <h6 style="color: #DD5347">Live Match</h6>


                    <!-- addQuestion -->
                    <div id="addQuestion" class="modal" style="overflow-y:scroll;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add Question </h5>

                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6>match : <span id="matchShow"></span></h6>


                                    <div id="addQustionSuccess">

                                    </div>


                                    <div class="field_wrapper-sub">

                                        <div class="form-group row">

                                            <div class="col-md-6">

                                                <input name="input_field" id="addQustionOfMatch"  placeholder="Enter Question" class="form-control gameType gameType-normal">

                                                <select name="input_field" id="addQustionOfMatch" class="form-control gameType gameType-football">
                                                    <option value="">Select a question</option>

                                                    <?php 
                                                        $q="select * from questions where game_type=1";
                                                        $r=mysqli_query($con,$q);
                                                        if ($r) {
                                                            while($row=mysqli_fetch_array($r)) 
                                                            {   
                                                    ?>
                                                    
                                                    <option value="<?php echo $row['content'] ?>"><?php echo $row['content'] ?></option>
                                                    <?php 
                                                            }
                                                        }
                                                    ?>
                                                </select>

                                                <select name="input_field" id="addQustionOfMatch" class="form-control gameType gameType-cricket">
                                                    <option value="">Select a question</option>

                                                    <?php 
                                                        $q="select * from questions where game_type=2";
                                                        $r=mysqli_query($con,$q);
                                                        if ($r) {
                                                            while($row=mysqli_fetch_array($r)) 
                                                            {   
                                                    ?>
                                                    
                                                    <option value="<?php echo $row['content'] ?>"><?php echo $row['content'] ?></option>
                                                    <?php 
                                                            }
                                                        }
                                                    ?>
                                                </select>

                                            </div>

                                        </div>
                                    </div>
                                    
                                    
                                    

                                    
                                    
                                    

                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input name="bettingId" id="bettingIdForAddQuestion" type="text"  value="" hidden="0">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input style="color: white; background-color: #996600; border: 1px solid #996600;" name="addBetSubTitle" id="addQuestionSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>

                                    
                                </div>
                                <div class="modal-footer">

                                    <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
        <!-- limitMatch -->
                    <div id="scoreModal" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add score </h5>

                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6>match : <span id="matchShowOfLimit"></span></h6>


                                    <div id="scoreSuc">

                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input class="form-control" name="limitRateAmount" id="ScoreRateForMatch"  value="">

                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input name="bettingId" id="matchIdForScore" type="text"  value="" hidden="1">
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input  style="color: white; background-color: #996600; border: 1px solid #996600;" name="limitBettingTitle" id="score" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer">

                                    <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- limitMatch -->
                    <div id="limitMatch" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add Limit </h5>

                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6>match : <span id="matchShowOfLimit"></span></h6>


                                    <div id="limitMatchSuccess">

                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input class="form-control" name="limitRateAmount" id="limitRateForMatch"  value="">

                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input name="bettingId" id="matchIdForLimit" type="text"  value="" hidden="1">
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <!-- <input id="new" name="limitBettingTitle" id="limitRateForMatchSubmit" type="submit" class="btn btn-success" value="submit"> -->
                                            <input name="limitBettingTitle" id="limitRateForMatchSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- wait -->
                    <div id="matchWatting" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Match Waiting Time </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <form method="post">
                                    <h6>match : <span id="matchShowOfWait"></span></h6>
                                    <div id="waitMatchSuccess">
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input class="form-control" id="matchWaittingRate" name="matchwaittime" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">                           
                                        <div class="col-md-8">
                                            <input name="bettingId" id="matchIdForWait" type="text"  value="" hidden="1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input id="new" name="waitBettingTitle" id="matchWattingTimeSubmit"type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>
                                </form>
                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- limitQuestion -->
                    <div id="limitQuestion" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add Limit </h5>

                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6 style="color: black;">Question  : <span id="questionShowOfLimit"></span></h6>


                                    <div id="limitQuestionSuccess">

                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input class="form-control" name="limitRateAmount" id="limitRateForQuestion"  value="">

                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input name="bettingId" id="questionIdForLimit" type="text"  value="" hidden="1">
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input id="new" name="limitBettingTitle" id="limitRateForQuestionSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- wait -->
                    <div id="questionWatting" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Question Waiting Time </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <form method="post">
                                    <h6>match : <span id="questionShowOfWait"></span></h6>
                                    <div id="waitQuestionSuccess">
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input class="form-control" id="questionWaittingRate" name="questionwatingtime" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">                           
                                        <div class="col-md-8">
                                            <input name="questionid" id="questionIdForWait" type="text"  value="" hidden="1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input id="new" name="waitBettingquestion" id="questionWattingTimeSubmit"type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>
                                </form>
                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- matchActionMenu -->
                    <div id="matchActionMenu" class="modal" style="">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">



                                <div class="modal-body">

                                    <div class="list-group">
                                        <a style="color: white; background-color: #996600; border: 1px solid #996600;" href="#" class="list-group-item btn btn-sm addQuestion" data-toggle="modal" data-target="#addQuestion">Add Question</a>
                                        <a style="color: white; background-color: #996600; border: 1px solid #996600;" href="#" class="list-group-item btn btn-sm updateMatch" data-toggle="modal" data-target="#updateMatch" data-dismiss="modal">Update match</a>
                                        
                                      <!--  <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="list-group-item btn btn-sm cancelMatch" onclick="return confirm('Are you sure?')">Cancel The Match</button>-->
                                        
                                        <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="list-group-item btn btn-sm closeMatch" onclick="return confirm('Are you sure?')">Close The Match</button>
                                    </div>


                                </div>

                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">X</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- hidden-match -->
                    <div id="hidden-section" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Hidden match</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body" id="hiddenSectionShow">

                                </div>
                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary m-default" type="button" data-dismiss="modal" id="">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->

                    <!-- matchActionMenu -->
                    <div id="default" class="modal" style="">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">



                                <div class="modal-body">

                                    <div class="list-group">
                                        <?php
                                        $query = "SELECT * FROM section";
                                        $resultreceivingMoneyNumber = $db->select($query);
                                        $i = 0;
                                        if ($resultreceivingMoneyNumber) {
                                            while ($receivingMoneyNumber = $resultreceivingMoneyNumber->fetch_assoc()) {

                                                $i++;
                                                ?>

                                                <a style="color: #996600;" href="#" class="list-group-item btn btn-sm section" g-type="2" id="<?php echo $receivingMoneyNumber['id']; ?>" data-toggle="modal" data-target="#hidden-section"> <?php echo $receivingMoneyNumber['title']; ?></a>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </div>


                                </div>

                                <div class="modal-footer">

                                    <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="btn btn-secondary" type="button" data-dismiss="modal">X</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->

                    <!-- Modal update match-->
                    <div id="updateMatch" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Match Title </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">


                                    <div id="UpdateMatchSuccess">

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">A Team</label>
                                        <div class="col-md-8">
                                            <input type="hidden" class="form-control matchIdForUpdate" id="" rows="4" value="" >
                                            <input type="text" class="form-control" id="Update_A_team" name="A_team" rows="4" value="" placeholder="Enter A Team Name">
                                             <span><input type="text" id="color_aup" name="color_aup" class="form-control" placeholder="Team Color"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">B Team</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control"  id="Update_B_team" rows="4" value="" placeholder="Enter  B Team Name">
                                             <span><input type="text" id="color_bup" name="color_bup" class="form-control" placeholder="Team Color"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">match Statement</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="title" id="Update_title" value="" rows="4" placeholder="Enter match Statement">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="Update_date" value="" name="date" type="text" placeholder="Select Date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Match Status</label>
                                        <div class="col-md-8">
                                            <!--Radio Button Markup-->
                                            <div class="">
                                                <label>
                                                    <input type="radio" name="Update_status" id="Update_status" value="1"><span class="label-text"> Live</span><br>
                                                    <input type="radio" name="Update_status" id="Update_status" value="2"><span class="label-text"> Upcoming</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Match Type</label>
                                        <div class="col-md-8">
                                            <!--Radio Button Markup-->
                                            <div class="">
                                                <label>
                                                    <input type="radio" name="Update_gameType" id="Update_gameType" value="1"><span class="label-text"> FootBall</span><br>
                                                    <input type="radio" name="Update_gameType" id="Update_gameType" value="2"><span class="label-text"> Cricket</span><br>
                                                    <input type="radio" name="Update_gameType" id="Update_gameType" value="3"><span class="label-text"> Basketball</span>
                                                   <br>

                                                          <input type="radio" name="Update_gameType" id="Update_gameType" value="6"><span class="label-text"> Horse Racing </span>
                                                          <br>
                                                          <input type="radio" name="Update_gameType" id="Update_gameType" value="7"><span class="label-text"> Badminton</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input style="color: white; background-color: #996600; border: 1px solid #996600;" name="updateMatch" id="updateMatchSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button style="color: white; background-color: #996600; border: 1px solid #996600;" class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- questionActionMenu -->
                    <div id="questionActionMenu" class="modal" style="">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">



                                <div class="modal-body">

                                    <div class="list-group">
                                        <a style="color: white; background-color: #996600; border: 1px solid #996600;" href="#" class="list-group-item btn btn-sm addAns" data-toggle="modal" data-target="#addAns">Add Answer</a>
                                        <a style="color: white; background-color: #996600; border: 1px solid #996600;" href="#" class="list-group-item btn btn-sm updateQuestion" data-toggle="modal" data-target="#updateQuestion" data-dismiss="modal">Update question</a>

                                        <button  style="color: white; background-color: #996600; border: 1px solid #996600;" class="list-group-item btn btn-sm closeQuestion" onclick="return confirm('Are you sure?')" >Close The question</button>
                                    </div>


                                </div>

                                <div class="modal-footer">

                                    <button id="new" class="btn btn-secondary" type="button" data-dismiss="modal">X</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- addAns Modal -->
                    <div id="addAns" class="modal" style="">
                        <div class="modal-dialog "  role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6>Question : <span id="questionShowOfAddAns"></span></h6>
                                    <div id="addAnsSuccess">
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input class="form-control" id="addAnsField"  placeholder="Enter Ans">
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control" id="addAnsRate"  placeholder="Rate">
                                        </div>
                                    </div>
                                    <div class="form-group row">                           
                                        <div class="col-md-8">
                                            <input  id="questionIdForAddAns" type="text"  value="" hidden="1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input  id="addAnsSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">

                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">X</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->
                    <!-- update-sub-betting Modal -->
                    <div id="updateQuestion" class="modal" style="">
                        <div class="modal-dialog "  role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Question </h5>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    

                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div id="UpdateQuestionSuccess">

                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Question</label>
                                        <div class="col-md-8">
                                            <input type="hidden" class="form-control questionIdForUpdate" id="" rows="4" value="" >
                                            <input class="form-control" id="editQuestion"  value="">
                                        </div>
                                    </div>
                                    
                                              
                              <div class="form-group row">

                                            <div class="col-md-6">
                                            
                                            <b>Ending time:</b>
                                              <input class="form-control" type="datetime-local" id="ending_time">

                                            
                                            </div>

                                        </div>


                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input name="updateMatch" id="updateQuestionSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">X</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->                                                       


                    <!-- edit betting rate -->
                    <div id="updateAnsRate" class="modal editRate" style="">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Answer </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div id="UpdateAnsSuccess">

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input class="form-control"  id="editAnswer" value="">
                                            <input type="hidden" class="form-control ansIdForUpdate" id="" rows="4" value="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input class="form-control rate"  id="editRateAmount" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input  id="updateAnsSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>  <!-- end Modal -->

                    <!-- limit betting rate -->
                    <div id="limitAns" class="modal" style="">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Limit Answer </h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <h6>Answer : <span id="ansShowOfLimit"></span></h6>
                                    <div id="limitAnsSuccess">
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input class="form-control" id="limitRateAmount"  value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-8">
                                            <input class="form-control"  id="ansIdForLimit"  value="" hidden="1">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-8">
                                            <input  id="limitRateForAnsSubmit" type="submit" class="btn btn-success" value="submit">
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">

                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- end Modal -->

                    <div id="liveMatchFetch">
                        <div class="loader loader-1">
                            <div class="loader-outter"></div>
                            <div class="loader-inner"></div>
                        </div>

                    </div>
                    <br> <h6 style="color: #17A05D">Upcoming Match</h6>  

                    <div id="upcomingContent">

                    </div>



                </div>
            </div>
        </div>
    </div>
</main>
<?php include './footer.php'; ?>

<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>

<!-- Page specific javascripts-->

<script>

                                            $(document).ready(function () {
                                                $("#liveMatchFetch").load('betLiveContent.php');
                                                $("#upcomingContent").load('upcomingMatch.php');
                                                var gameid=$('#bettingIdForAddQuestion').val();
                                                $.ajax({
                                                    url: 'autoquestion.php',
                                                    type: 'POST',
                                                 
                                                    data: {game_id:gameid},
                                                   success: function(data) {
                                                       //alert(data);
                                                    },
                                                })
                                               
                                                
                                            });

                                            $(document).on('click', '#hidden', function (event) {
                                                $("#hiddenContentShow").load('hiddenContent.php');

                                            });
</script>
<!-- won-->  
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->

<script type="text/javascript" src="js/plugins/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/plugins/select2.min.js"></script>


<!-- Material Datepicker plugin -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript" src="js/betAction.js"></script>

<script>
$(document).ready(function() {
    // $('#date').bootstrapMaterialDatePicker({ weekStart : 0, time: false, format : 'DD MMMM h:m A' });
    // $('#time').bootstrapMaterialDatePicker({ date: false, format : 'h:m A' });
    $('#date').bootstrapMaterialDatePicker({ format : 'DD MMMM h:m A' });
    $('#Update_date').bootstrapMaterialDatePicker({ format : 'DD MMMM h:m A' });
    $.material.init();
})
                                            $(function () {

                                                setInterval(function () {
                                                    if ($("#success").is(":visible")) {
                                                        //you may add animate.css class for fancy fadeout
                                                        $("#success").fadeOut("fast");
                                                    }
                                                }, 10000);

                                            });

                                            $('.editRate').on('shown.bs.modal', function () {
                                                $(this).find('.rate').focus();
                                            });


                                            setInterval(function () {
                                                $.ajax({
                                                    url: "betRefresh.php",
                                                    success: function (data) {

                                                        if (data === "1") {
                                                            $("#liveMatchFetch").load('betLiveContent.php');
                                                            $("#upcomingContent").load('upcomingMatch.php');
                                                        }
                                                    }
                                                });
                                            }, 60000);
                                            $(document).on('click', '.gameTypec', function () {

                                                $('.hiddenOp').toggle();


                                            });

</script>

<?php
if(isset($_POST['waitBettingTitle']))
{
    $match_id=$_POST['bettingId'];
    $waiting=$_POST['matchwaittime'];
    $q="update betting_title set waittingTime='$waiting' where id='$match_id'";
    if(mysqli_query($con,$q))
    {

    }
}

if(isset($_POST['waitBettingquestion']))
{
    $questionid=$_POST['questionid'];
    $waiting=$_POST['questionwatingtime'];
    $q="update betting_subtitle set waittingTime='$waiting' where id='$questionid'";
    if(mysqli_query($con,$q))
    {

    }
}

?>
</body>

</html>