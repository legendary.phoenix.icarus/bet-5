<?php include './header.php'; ?>

<!-- javascript for multibet -->
<script>
var games = [];
var map = [];
var RATE = 0;
var index = 0;
function updateMB() {
  if (games != []) {
    document.getElementById("mb").style.display = "inline";
  } else {
    document.getElementById("mb").style.display = "none";
  }

  document.getElementById("mb_total").innerHTML = games.length;
  document.getElementById("mb_rate").innerHTML = RATE;
  document.getElementById("mb_return").innerHTML = parseFloat(
    document.getElementById("mb_amount").value * RATE
  );
}

function helper(val) {
}

function removeFromCart(i) {
    RATE -= games[i].betRate;
  var m = map.indexOf(games[i].betId);
  games.splice(i, 1);
  map.splice(m, 1);

  updateMB();
  showSelected();
}

function addToCart() {
  var found = false;

  var match = $("#match").val();
  var matchBet = $("#matchBet").val();
  var betId = $("#betId").val();
  var matchId = $("#matchId").val();
  var betTitleId = $("#betTitleId").val();
  var betRate = $("#betRate").val();

  if (
    match == "" ||
    matchBet == "" ||
    betId == "" ||
    matchId == "" ||
    betTitleId == "" ||
    betRate == ""
  ) {
    return;
  }

  var hid = "#headingOne" + matchId + " h4";
  var info = document.querySelector(hid).innerHTML.trim();

  var game = {
    matchInfo: info,
    match: match,
    matchBet: matchBet,
    betRate: betRate,
    betId: betId,
    matchId: matchId,
    betTitleId: betTitleId,
  };

  for (var i = 0; i < map.length; i++) {
    if (map[i] == matchId) {
      found = true;
      break;
    } else {
      found = false;
    }
  }

  if (found == true) {
    alert("Error! May be you have already added this game!");
    found = false;
  } else {
    games.push(game);
    map.push(matchId);
    RATE += parseFloat(betRate);

    document.getElementById("addBetLoad").style.display = "block";

    setTimeout(function () {
      document.getElementById("addBetLoad").style.display = "none";
    }, 1000);
  }

  updateMB();
}

function showSelected() {
  var showSelected_ = document.getElementById("showSelected_");
  showSelected_.innerHTML = "";
  for (var i = 0; i < games.length; i++) {
    var hid = "headingOne" + games[i].matchId;
    var q = document.getElementById(hid).innerHTML;

    // console.log(q, hid, games[i].matchId);

    showSelected_.innerHTML +=
      `        <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">` +
      q +
      `</h5>
        <p class="card-text">` +
      "Question: " +
      games[i].match +
      "<br> Your Answer: " +
      games[i].matchBet +
      `</p>
        <a onclick="removeFromCart(` +
      i +
      `)" class="btn btn-primary">Remove</a>
      </div>
    </div>
  </div>`;
  }
}

function placeTheMultiBet() {
  if (
    $("#mb_amount").val() == "" ||
    $("#mb_amount").val() < 20 ||
    $("#mb_amount").val() > 100
  ) {
    alert("Amount must be greater than 20tk and less than 100tk.");
    return;
  } else if (games == "") {
    alert("Please select at least 1 game for the multibet!");
    return;
  }
  document.getElementById("multiBetSubmit").innerHTML =
    "Submiting your request!...";
  document.getElementById("multiBetSubmit").disabled = true;
  var jsonDeatils = { details: games };
  $.ajax({
    method: "POST",
    url: "multiBet.php",
    data: {
      details: JSON.stringify(jsonDeatils),
      total: games.length,
      betAmount: $("#mb_amount").val(),
      rate: RATE,
    },
    success: function (data) {
      alert(data);
      location.reload();
    //   console.log(data);
    },
  });
}

</script>


<link rel="stylesheet" href="css/indexStyle.css">

<link href="https://fonts.googleapis.com/css?family=Palanquin&display=swap" rel="stylesheet">



<style>
body{
    font-family: arial;
    margin: 0;
}
.tabBlock
{
    background-color:#57574f;
    border:solid 0px #FFA54F;
    border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;
    max-width:200px;
    width:100%;
    overflow:hidden;
    display:block;
}
.clock
{
    vertical-align:middle;
    font-family:Orbitron;
    font-size:40px;
    font-weight:normal;
    color:#FFF;
    padding:0 10px;
}
.clocklg 
{
    vertical-align:middle; 
    font-family:Orbitron;
    font-size:20px;
    font-weight:normal;
    color:#FFF;
}
</style>
    
    
    
    
        <!-- Modal withdraw -->
        <div id="withdraw" class="modal fade" role="dialog" >
            <div class="modal-dialog  " >

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header m-head" style="  background: #017B5B !important;">

                        <button type="button" class="close" data-dismiss="modal" style="color: #ffffff">&times;</button>
                        <h4 class="modal-title" style="color: black"> &nbsp;Request a withdraw</h4>
                    </div>
                    <div class="modal-body" style="padding: 2% !important">
                        <div class="">
                            <div role="form" class="register-form">
<strong> Minimum Withdraw 500 TK !!</strong>


<div id="errorWithraw" class="alert alert-danger errorWithraw" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        ×</button>  <strong>  Opps !!</strong> <span id="errorWithrawText"></span>
                                </div>


                                <hr class="colorgraph">
                                <div class="row">
                  

                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">

                                        <div class="form-group">
                                            <label style="text-align: left;width: 100%;padding-left:2px;">Amount <span style="color:#DD4F43;">*</span></label>
                                            <input type="text" name="first_name" id="wAmount-c" class="form-control input-lg" placeholder="Amount" tabindex="1">
                                        </div>
                                    </div>
                                    <!---->
                                    <div class="col-xs-12 col-sm-6 col-md-6">

                                        <div class="form-group">
                                            <label style="text-align: left;width: 100%;">Number <span style="color:#DD4F43;">*</span></label>
                                            <input type="text" name="number_" id="number_c_" class="form-control input-lg" placeholder="Number" tabindex="1">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <label style="text-align: left;width: 100%;">Password <span style="color:#DD4F43;">*</span></label>
                                        <div class=
                                        "form-group">
                                            <input type="text" name="password" id="password-c" class="form-control input-lg" placeholder="Password" tabindex="1">
                                        </div>
                                    </div>

                                </div>

                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6"><input type="submit" id="withdrawSubmit-c" value="Submit" class="btn btn-success btn-block btn-lg" tabindex="7"></div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        
    
    
<section class="callaction " style="min-height: 450px;">

    <div class="content-container mx-auto p-0">

        <div class="container">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12  pl-0 pr-0 ">

                <div class="list-group" >
                    <?php
                    $query = "SELECT * FROM `notice`";
                    $result = $db->select($query);
                    $notice = $result->fetch_assoc();
                    ?>
                    <marquee style="color: #C4C4C4;background: #203F61;background-image: linear-gradient(to right top, #ffffff, #f5fcff, #e2fbff, #d1fbf9, #cef9e2);color:#795BDF" class="mrq" scrollamount='3' direction="scroll"> <?php echo $notice['text']; ?></marquee>

                        <div class="tab-content ">
                            <div class="tab-pane active" id="1">

                            </div>
                            <div class="tab-pane" id="2">

                            </div>
                            <div class="tab-pane" id="3">

                            </div>
                        
                        </div>
                    </div>
            </div>
        </div>

        <div class="container">                     
          <a style="background: #3D7EA6 ; color:yellow; font-weight:bolder"  class="list-group-item"><span class=""> <i class="fa-spin"></i> Live Match</span><span class="glyphicon glyphicon-menu-right mg-icon pull-right"></span></a><hr>
            
          <div id="liveContent">
              
          </div>

          <hr>
          <a style=" background: #3D7EA6;color:yellow; font-weight:bolder;"  class="list-group-item"><span class=""><i class="fa-spin"></i> Upcoming Match</span><span class="glyphicon glyphicon-menu-right mg-icon pull-right"></span></a><hr>
          
          <div id="upcomingContent">
            
          </div>
          <hr>
        </div>
    </div><!-- ./ end row -->
</section>


<div id="dataModal" class="modal fade">  
    <div class="modal-dialog">  
        <div class="modal-content">  
            <div class="modal-header">  
                <button type="button" class="close" data-dismiss="modal">&times;</button>  
                <h4 class="modal-title">Employee Details</h4>  
            </div>  
            <div class="modal-body" id="employee_detail">  
            </div>  
            <div class="modal-footer">  
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div>  
        </div>  
    </div>  
</div>  
<!-- Modal betting-->

<div class="modal fade betForm" id="betting" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content m-content">
            <div class="modal-header m-head" style="  background: #3D7EA6 !important;">

                <button type="button" class="close" data-dismiss="modal" style="color: #ffffff">&times;</button>
                <h4 class="modal-title" style="color: #D2D2D2">  &nbsp; Place Bet</h4>
            </div>
            <div class="modal-body" style="padding: 2% !important">
                <div class="signup-form">

                    <div  id="formData">

                        <div id="errorPlaceBet" class="alert alert-danger errorPlaceBet" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                ×</button>  <strong>  Opps !!</strong> <span id="PlaceBetText"></span>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-7">
                                    <label><span id="BettingSubTitleOption"></span> <span id="betRateShow" class="badge text-right"></span></label> <br>
                                    <label><small id="bettingSubTitle"></small></label><br>
                                    <label>
                                        <span id="bettingTitle"></span>
                                        <span  id="gameLiveOrUpcoming"  class="badge text-right"></span>

                                    </label><br>
                                    <input type="text"  id="match" value="" hidden="1">
                                    <input type="text"  id="matchBet"value="" hidden="1">
                                    <input type="text"  id="betRate"value="" hidden="1">
                                    <input type="text"  id="betId"value="" hidden="1">
                                    <input type="text"  id="matchId" value="" hidden="1">
                                    <input type="text"  id="betTitleId" value="" hidden="1">

                                    <label class="gameLogo">

                                        <img id="gameLogo" style="box-shadow: 1px 7px 4px 0px #787981 !important;border-radius: 50%;" class="img-circle" src="" width="25px;">&nbsp;


                                    </label>

                                </div>
                                <div class="col-lg-5">

                                    <input type="text" class="form-control"   id="stakeAmount" value="" >


                                </div>

                            </div>



                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-7">
                                    <label><strong>Total Stake</strong></label>

                                </div>
                                <div class="col-lg-5">
                                    <label class="col-lg-12 text-right"><strong id="stakeAmountView">100</strong></label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-7">
                                    <label><strong>Possible Winning</strong></label>

                                </div>
                                <div class="col-lg-5">
                                    <label class="col-lg-12 text-right"><strong id="possibleAmount"> 100.00</strong></label>

                                </div>
                            </div>
                        </div>


                        <div class="form-group">

                            <button  type="submit" id="placeBet"  name="placeBet"  class="btn btn-success btn-lg btn-block">Place Bet</button>
                        <!-- <center><b>Or,</b></center>
                            <br>
                            
                            <button class="btn btn-info btn-lg btn-block" style="background: #3D7EA6;" id="addBet" onclick="addToCart()">Select for multibet</button> 

                            <button  id="load" class="btn btn-success btn-sm btn-block load"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></button>-->
                        </div>
                        
                        <!--<button  id="addBetLoad" class="btn btn-success btn-sm btn-block load"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></button>-->
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
                       
<footer class="bg-primary text-white text-center text-lg-start">
  <!-- Grid container -->
  <!-- Copyright -->
  <div class="text-center p-3 right-reserved">
    <img style="width: 134px;height: 48px;" src="img/logo.png"><br/>
    <spam style="color:orange"> gamet20</spam> © <?=date('Y')?> all right reserved.
  </div>
  <!-- Copyright -->
</footer>
</div>


<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min_1.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/validation/placeBet.js"></script>

<script src="js/validation/validated.js"></script>
<script src="js/validation/siteRefresh.js"></script>
<script src="js/validation/deposit_and_withdraw.js"></script>


<script>
$(document).ready(function () {
     $("#liveContent").load('content.php');
     $("#upcomingContent").load('upcomingMatch.php');

});
</script>
<?php
if (isset($_COOKIE["userId"]) AND ( isset($_COOKIE["password"]))) {

    include './chatBox.php';
}
?>


<style type="text/css">
    .dateshow {
    text-align: center;
    padding: 4px 0px;
    color: #C4C4C4;
    background:#022644;
    border-left: 2px solid #D8AB7D;
    border-right: 2px solid #D8AB7D;
    font-size: 15px;
    border-bottom: 1px solid #9E7143;
    border-left: 2px solid #D8AB7D;
    border-right: 2px solid #D8AB7D;
    font-size: 15px;
    border-bottom: 1px solid #9E7143;
    margin-top: -3px;
    margin-bottom: 4px;
    font-style: arial;
    font-weight: bold;
}
</style>