-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2021 at 09:41 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(12) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `balance` double NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 1,
  `refresh` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `userName`, `password`, `balance`, `type`, `refresh`) VALUES
(1, 'admin', 'admin@gamet20.com', 'admin', '777230', 1243022, 1, 0),
(12, 'adminadmin', '', 'admin2', '77230', 0, 2, 0),
(14, 'ss', '', 'sky', '120', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admintransaction`
--

CREATE TABLE `admintransaction` (
  `id` int(11) NOT NULL,
  `debit` double DEFAULT 0,
  `credit` double NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `total` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admintransaction`
--

INSERT INTO `admintransaction` (`id`, `debit`, `credit`, `description`, `time`, `total`) VALUES
(1, 100000, 0, 'deposit by test', '2021-05-30 15:47:36', 1350000),
(2, 5000, 0, 'deposit by sky', '2021-05-30 19:32:11', 1345000),
(3, 0, 0, 'withrawal by ksajib', '2021-05-30 20:32:14', 1345000),
(4, 0, 0, 'withrawal by ksajib', '2021-06-03 18:25:10', 1345000),
(5, 58000, 0, 'deposit by shamim123', '2021-06-03 19:16:03', 1287000),
(6, 200, 0, 'deposit by sky', '2021-06-05 10:34:38', 1286800),
(7, 200, 0, 'deposit by sky', '2021-06-05 19:04:23', 1286600),
(8, 50000, 0, 'deposit by sky', '2021-06-06 14:23:09', 1236600),
(9, 0, 1000, 'withrawal by test', '2021-06-06 18:51:02', 1237600),
(10, 5000, 0, 'deposit by sky', '2021-06-07 01:39:29', 1232600),
(11, 0, 4000, 'withrawal by sky', '2021-06-07 01:39:46', 1236600),
(12, 0, 5000, 'withrawal by sky', '2021-06-07 04:15:11', 1241600),
(13, 0, 5000, 'withrawal by sky', '2021-06-07 04:15:20', 1246600),
(14, 5000, 0, 'deposit by sky', '2021-06-07 04:33:12', 1241600),
(15, 0, 1122, 'withrawal by sky', '2021-06-07 17:08:48', 1242722),
(16, 500, 0, 'deposit by sky', '2021-07-02 19:04:07', 1242222),
(17, 500, 0, 'deposit by sky', '2021-07-02 19:04:18', 1241722),
(18, 0, 1300, 'withrawal by sky', '2021-07-06 19:47:09', 1243022);

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `deposit` double NOT NULL DEFAULT 0,
  `withdraw` double NOT NULL DEFAULT 0,
  `notificationType` int(11) NOT NULL DEFAULT 0,
  `pay_method` varchar(20) NOT NULL DEFAULT '0',
  `acc_type` varchar(20) NOT NULL DEFAULT '0',
  `from_number` varchar(20) NOT NULL DEFAULT '0',
  `to_number` varchar(20) NOT NULL DEFAULT '0',
  `ref_number` varchar(20) NOT NULL DEFAULT '0',
  `time` varchar(50) DEFAULT NULL,
  `seen` int(11) NOT NULL DEFAULT 0,
  `action` int(11) NOT NULL DEFAULT 1,
  `wAction` int(11) NOT NULL DEFAULT 1,
  `confirm_number` varchar(20) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT 0,
  `notes` varchar(5000) NOT NULL DEFAULT '0',
  `actionAt` varchar(50) NOT NULL DEFAULT '0',
  `userType` int(11) NOT NULL DEFAULT 0,
  `userBalance` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_notification`
--

INSERT INTO `admin_notification` (`id`, `userId`, `deposit`, `withdraw`, `notificationType`, `pay_method`, `acc_type`, `from_number`, `to_number`, `ref_number`, `time`, `seen`, `action`, `wAction`, `confirm_number`, `rate`, `notes`, `actionAt`, `userType`, `userBalance`) VALUES
(1, 'test', 100000, 0, 1, 'bKash Personal', '0', '01979744444', 'ketaedvfdfsdft', '01900000000', '30 May 2021 9:42 PM', 1, 1, 1, '0', 1, '0', '30 May 2021 9:47 PM', 0, 5000),
(2, 'sky', 5000, 0, 1, 'bKash Personal', '0', '01600000000', 'jhgjgjh', '01900000000', '31 May 2021 1:27 AM', 1, 1, 1, '0', 1, '0', '31 May 2021 1:32 AM', 0, 104100),
(3, 'ksajib', 0, 5000, 2, '0', '0', '0', '0', '0', '31 May 2021 2:31 AM', 1, 1, 1, '', 0, '0', '31 May 2021 2:32 AM', 2, 107900),
(4, 'sky', 0, 1000, 2, 'bKash ', 'Personal', '0', '01713564348', '0', '03 Jun 2021 12:57 AM', 0, 1, 3, '0', 1, 'mjhjhjh', '0', 0, 0),
(5, 'ksajib', 0, 5000, 2, 'bKash ', 'Personal', '0', '01713564348', '0', '04 Jun 2021 12:22 AM', 1, 1, 1, '', 0, '0', '04 Jun 2021 12:25 AM', 2, 156337.5),
(6, 'shamim123', 5000, 0, 1, 'bKash Personal', '0', '32456', 'kjhkjhk', '01900000000', '04 Jun 2021 1:14 AM', 1, 1, 1, '0', 1, '0', '04 Jun 2021 1:16 AM', 0, 156337.5),
(7, 'sky', 200, 0, 1, 'bKash Personal', '0', '3567', '1234', '01900000000', '05 Jun 2021 4:25 PM', 1, 1, 1, '0', 1, '0', '05 Jun 2021 4:34 PM', 0, 214337.5),
(8, 'sky', 200, 0, 1, 'Bkash', '0', '12', '00', '1', '06 Jun 2021 12:05 AM', 1, 3, 1, '0', 1, '0', '06 Jun 2021 1:04 AM', 0, 214437.5),
(9, 'sky', 200, 0, 1, 'Bkash', '0', '12', '12', '1', '06 Jun 2021 12:33 AM', 1, 1, 1, '0', 1, '0', '06 Jun 2021 1:04 AM', 0, 214437.5),
(10, 'sky', 0, 2000, 2, 'Bkash', 'Personal', '0', 'admin', '0', '06 Jun 2021 12:39 AM', 0, 1, 3, '0', 1, '0', '0', 0, 0),
(11, 'sky', 50000, 0, 1, 'Bkash', '0', '01700000000', 'THIS FIELD IS HIDDEN', '1', '06 Jun 2021 8:22 PM', 1, 1, 1, '0', 1, '0', '06 Jun 2021 8:23 PM', 0, 214100.5),
(12, 'test', 0, 1000, 2, 'Bkash', 'Personal', '0', '01971458963', '0', '07 Jun 2021 12:50 AM', 1, 1, 1, 'Select number', 1, '0', '07 Jun 2021 12:51 AM', 0, 262800.5),
(13, 'sky', 5000, 0, 1, 'Bkash', '0', '01750000040', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 7:38 AM', 1, 1, 1, '0', 1, '0', '07 Jun 2021 7:39 AM', 0, 258800.5),
(14, 'sky', 0, 4000, 2, 'Bkash', 'Personal', '0', '01713564348', '0', '07 Jun 2021 7:39 AM', 1, 1, 1, 'Select number', 1, '0', '07 Jun 2021 7:39 AM', 0, 263800.5),
(15, 'sky', 400, 0, 1, 'Bkash', '0', '23533', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 7:47 AM', 1, 3, 1, '0', 1, '0', '07 Jun 2021 10:33 AM', 0, 253700.5),
(16, 'sky', 2000, 0, 1, 'Bkash', '0', '2545455', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 8:03 AM', 1, 3, 1, '0', 1, '0', '07 Jun 2021 10:33 AM', 0, 253700.5),
(17, 'sky', 0, 5000, 2, 'Bkash', 'Personal', '0', '32635415', '0', '07 Jun 2021 10:14 AM', 1, 1, 1, 'Select number', 1, '0', '07 Jun 2021 10:15 AM', 0, 253700.5),
(18, 'sky', 0, 5000, 2, 'Bkash', 'Personal', '0', '32635415', '0', '07 Jun 2021 10:14 AM', 0, 1, 3, '0', 1, '0', '0', 0, 0),
(19, 'sky', 5000, 0, 1, 'Bkash', '0', '65454', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 10:14 AM', 1, 1, 1, '0', 1, '0', '07 Jun 2021 10:33 AM', 0, 253700.5),
(20, 'sky', 5000, 0, 1, 'Bkash', '0', '65454', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 10:14 AM', 1, 1, 1, '0', 1, '0', '07 Jun 2021 10:33 AM', 0, 253700.5),
(21, 'sky', 5000, 0, 1, 'Bkash', '0', '23123', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 10:34 AM', 1, 3, 1, '0', 1, '0', '03 Jul 2021 1:04 AM', 0, 257678.5),
(22, 'sky', 5000, 0, 1, 'Bkash', '0', '56456', 'THIS FIELD IS HIDDEN', '1', '07 Jun 2021 10:43 AM', 1, 3, 1, '0', 1, '0', '03 Jul 2021 1:04 AM', 0, 257678.5),
(23, 'sky', 0, 1122, 2, 'Bkash', 'Personal', '0', '1122', '0', '07 Jun 2021 11:08 PM', 1, 1, 1, 'Select number', 1, '0', '07 Jun 2021 11:08 PM', 0, 262578.5),
(24, 'sky', 0, 1122, 2, 'Bkash', 'Agent', '0', '1122', '0', '07 Jun 2021 11:09 PM', 0, 1, 3, '0', 1, '0', '0', 0, 0),
(25, 'sky', 500, 0, 1, 'Bkash', '0', '0123456789', 'THIS FIELD IS HIDDEN', '1', '30 Jun 2021 5:52 AM', 1, 1, 1, '0', 1, '0', '03 Jul 2021 1:04 AM', 0, 257678.5),
(26, 'sky', 5000, 0, 1, 'Bkash', '0', '017000000000', 'THIS FIELD IS HIDDEN', '1', '07 Jul 2021 1:25 AM', 0, 1, 1, '0', 1, '0', '0', 0, 0),
(27, 'sky', 0, 1300, 2, 'Bkash', 'Agent', '0', '01700000', '0', '07 Jul 2021 1:29 AM', 1, 1, 1, 'Select number', 1, '0', '07 Jul 2021 1:47 AM', 0, 52831),
(28, 'sky', 0, 1300, 2, 'Bkash', 'Personal', '0', '01700000', '0', '07 Jul 2021 1:30 AM', 0, 1, 3, '0', 1, '0', '0', 0, 0),
(29, 'sky', 0, 1000, 2, 'Bkash', 'Personal', '0', '1000', '0', '07 Jul 2021 1:47 AM', 0, 1, 1, '0', 1, '0', '0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_notify`
--

CREATE TABLE `admin_notify` (
  `id` int(12) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balance_transfer`
--

CREATE TABLE `balance_transfer` (
  `id` int(12) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `to_userId` varchar(50) NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `time` varchar(50) DEFAULT NULL,
  `notes` varchar(500) DEFAULT '1',
  `userBalance` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balance_transfer`
--

INSERT INTO `balance_transfer` (`id`, `userId`, `to_userId`, `amount`, `time`, `notes`, `userBalance`) VALUES
(1, 'sky', 'test', 1000, '31 May 2021 1:35 AM', 'kmjnkl', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bet`
--

CREATE TABLE `bet` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `club` varchar(50) NOT NULL DEFAULT '0',
  `betTitle` varchar(100) NOT NULL DEFAULT '0',
  `betTitleId` int(11) NOT NULL DEFAULT 0,
  `betId` int(11) NOT NULL DEFAULT 0,
  `matchId` int(11) NOT NULL DEFAULT 0,
  `matchTitle` varchar(50) NOT NULL DEFAULT '0',
  `betRate` double NOT NULL DEFAULT 0,
  `betAmount` int(11) NOT NULL DEFAULT 0,
  `betStatus` int(11) NOT NULL DEFAULT 0,
  `time` varchar(500) DEFAULT NULL,
  `returnAmount` double NOT NULL DEFAULT 1,
  `notes` varchar(500) NOT NULL DEFAULT '1',
  `action` int(11) NOT NULL DEFAULT 0,
  `persentage` double NOT NULL DEFAULT 0,
  `actionAt` varchar(50) NOT NULL DEFAULT '1',
  `betstatusby` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bet`
--

INSERT INTO `bet` (`id`, `userId`, `club`, `betTitle`, `betTitleId`, `betId`, `matchId`, `matchTitle`, `betRate`, `betAmount`, `betStatus`, `time`, `returnAmount`, `notes`, `action`, `persentage`, `actionAt`, `betstatusby`) VALUES
(1, 'test', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 1, '30 May 2021 11:35 PM', 200, '1', 0, 0, '30 May 2021 11:37 PM', 'Wined by admin at 05.30.21, Time: 11:37:48'),
(2, 'test', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 1000, 2, '30 May 2021 11:35 PM', 1250, '1', 0, 0, '1', 'Wined by admin at 05.30.21, Time: 11:37:48'),
(3, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(4, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(5, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(6, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(7, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(8, 'sky', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 100, 1, '31 May 2021 1:48 AM', 125, '1', 0, 0, '1', NULL),
(9, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(10, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(11, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(12, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(13, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(14, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:48 AM', 200, '1', 0, 0, '1', NULL),
(15, 'Mhasanbdfp', 'ksajib', 'Wolfsberger AC', 2, 3, 1, 'Wolfsberger AC', 1.25, 1000, 0, '31 May 2021 2:53 AM', 1250, '1', 0, 0, '1', NULL),
(16, 'sky', 'ksajib', 'Austria Wien', 2, 4, 1, 'Wolfsberger AC', 2, 100, 0, '31 May 2021 1:27 PM', 200, '1', 0, 0, '1', NULL),
(17, 'sky', 'ksajib', '1 goal', 6, 5, 5, 'Rahim', 1.25, 50, 1, '01 Jun 2021 2:43 AM', 62.5, '1', 0, 0, '01 Jun 2021 2:50 AM', 'Wined by admin at 06.01.21, Time: 02:50:14'),
(18, 'sky', 'ksajib', '0 goal', 6, 12, 5, 'Rahim', 2, 100, 2, '01 Jun 2021 2:43 AM', 200, '1', 0, 0, '1', 'Wined by admin at 06.01.21, Time: 02:50:15'),
(19, 'sky', 'ksajib', '2 goal', 7, 9, 5, 'karim', 2, 100, 1, '01 Jun 2021 2:43 AM', 200, '1', 0, 0, '01 Jun 2021 2:50 AM', 'Wined by admin at 06.01.21, Time: 02:50:46'),
(20, 'sky', 'ksajib', '0 goal', 7, 13, 5, 'karim', 1.5, 100, 2, '01 Jun 2021 2:43 AM', 150, '1', 0, 0, '1', 'Wined by admin at 06.01.21, Time: 02:50:49'),
(21, 'sky', 'ksajib', '1 goal', 7, 14, 5, 'karim', 2.5, 100, 2, '01 Jun 2021 2:43 AM', 250, '1', 0, 0, '1', 'Wined by admin at 06.01.21, Time: 02:50:49'),
(22, 'sky', 'ksajib', '1-1', 8, 10, 5, 'Half time score', 1, 100, 1, '01 Jun 2021 2:43 AM', 100, '1', 0, 0, '01 Jun 2021 2:51 AM', 'Wined by admin at 06.01.21, Time: 02:51:56'),
(23, 'sky', 'ksajib', '2.5 over', 9, 15, 5, 'full time score', 1.25, 100, 1, '01 Jun 2021 2:48 AM', 125, '1', 0, 0, '01 Jun 2021 2:52 AM', 'Wined by admin at 06.01.21, Time: 02:52:26'),
(24, 'sky', 'ksajib', '1 goal', 7, 14, 5, 'karim', 2.5, 100, 2, '01 Jun 2021 2:48 AM', 250, '1', 0, 0, '1', 'Wined by admin at 06.01.21, Time: 02:50:49'),
(25, 'sky', 'ksajib', 'England', 10, 16, 7, 'Full Time', 1.5, 200, 1, '03 Jun 2021 1:10 AM', 300, '1', 0, 0, '1', NULL),
(26, 'sky', 'ksajib', '2-2', 9, 11, 5, 'full time score', 2, 100, 0, '05 Jun 2021 11:57 PM', 200, '1', 0, 0, '1', NULL),
(27, 'sky', 'ksajib', 'bangladesh', 23, 42, 13, 'To win the match', 1.6, 537, 1, '06 Jun 2021 4:51 PM', 859.2, '1', 0, 0, '1', NULL),
(28, 'sky', 'ksajib', 'draw', 15, 28, 8, 'Full Time Result ', 3, 100, 0, '06 Jun 2021 8:25 PM', 300, '1', 0, 0, ' ', 'Wined by admin at 06.08.21, Time: 11:30:31'),
(29, 'sky', 'ksajib', 'draw', 15, 28, 8, 'Full Time Result ', 3, 100, 0, '06 Jun 2021 8:25 PM', 300, '1', 0, 0, ' ', 'Wined by admin at 06.08.21, Time: 11:30:31'),
(30, 'sky', 'ksajib', 'draw', 15, 28, 8, 'Full Time Result ', 3, 100, 0, '06 Jun 2021 8:25 PM', 300, '1', 0, 0, ' ', 'Wined by admin at 06.08.21, Time: 11:30:31'),
(31, 'sky', 'ksajib', 'bangladesh', 15, 27, 8, 'Full Time Result ', 2, 100, 0, '07 Jun 2021 9:52 AM', 200, '1', 0, 0, ' ', 'Wined by admin at 06.08.21, Time: 11:30:31'),
(32, 'sky', 'ksajib', 'bangladesh', 15, 27, 8, 'Full Time Result ', 2, 5000, 0, '08 Jun 2021 11:28 PM', 10000, '1', 0, 0, ' ', 'Wined by admin at 06.08.21, Time: 11:30:31'),
(33, 'sky', 'ksajib', 'bangladesh', 16, 30, 8, 'Half Time Result ', 2.5, 100, 0, '28 Jun 2021 1:52 AM', 250, '1', 0, 0, '1', NULL),
(34, 'sky', 'ksajib', 'B', 22, 41, 12, 'To Win the match', 1, 100, 0, '28 Jun 2021 3:01 AM', 100, '1', 0, 0, '1', NULL),
(35, 'sky', 'ksajib', 'bangladesh', 15, 27, 8, 'Full Time Result ', 2, 100, 0, '29 Jun 2021 4:22 AM', 200, '1', 0, 0, '1', NULL),
(36, 'sky', 'ksajib', 'England', 28, 50, 18, 'To Win The Toss', 1.9, 100, 0, '29 Jun 2021 9:05 PM', 190, '1', 0, 0, ' ', 'Wined by admin at 06.29.21, Time: 09:06:17'),
(37, 'sky', 'ksajib', ' Tsvetkov E', 25, 46, 16, '1st Set Win ', 2, 3000, 1, '30 Jun 2021 12:36 AM', 6000, '1', 0, 0, '30 Jun 2021 12:37 AM', 'Wined by admin at 06.30.21, Time: 12:37:21'),
(38, 'sky', 'ksajib', ' Sinkovskiy V', 25, 47, 16, '1st Set Win ', 1.7, 2500, 2, '30 Jun 2021 12:36 AM', 4250, '1', 0, 0, '1', 'Wined by admin at 06.30.21, Time: 12:37:21'),
(39, 'sky', 'ksajib', 'Kovalchuk N', 33, 57, 20, 'To Win The Match ', 1.5, 578, 1, '03 Jul 2021 9:40 PM', 867, '1', 0, 0, '1', NULL),
(40, 'sky', 'ksajib', 'Newcastle Olympic FC', 35, 61, 22, 'Full Time Result', 2.2, 100, 1, '05 Jul 2021 3:01 AM', 220, '1', 0, 0, '1', NULL),
(41, 'sky', 'ksajib', 'Kuzyshyn O', 48, 89, 34, '1st Set Winners', 1.81, 1000, 1, '06 Jul 2021 10:44 PM', 1810, '1', 0, 0, '06 Jul 2021 10:45 PM', 'Wined by admin at 07.06.21, Time: 10:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `betclosehistory`
--

CREATE TABLE `betclosehistory` (
  `id` int(12) NOT NULL,
  `matchId` int(11) NOT NULL,
  `queId` int(11) NOT NULL DEFAULT 0,
  `gainAmunt` double NOT NULL DEFAULT 0,
  `SendingAmount` double NOT NULL DEFAULT 0,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betclosehistory`
--

INSERT INTO `betclosehistory` (`id`, `matchId`, `queId`, `gainAmunt`, `SendingAmount`, `time`) VALUES
(1, 1, 1, 0, 0, '2021-05-30 16:12:57'),
(2, 1, 1, 0, 0, '2021-05-30 16:13:01'),
(3, 1, 1, 0, 0, '2021-05-30 16:32:42'),
(4, 1, 2, 0, 0, '2021-05-30 16:43:10'),
(5, 1, 2, 0, 0, '2021-05-30 16:43:14'),
(6, 1, 2, 0, 0, '2021-05-30 16:43:19'),
(7, 1, 2, 0, 0, '2021-05-30 16:45:36'),
(8, 1, 2, 0, 0, '2021-05-30 17:37:48'),
(9, 5, 6, 0, 0, '2021-05-31 20:50:14'),
(10, 5, 6, 0, 0, '2021-05-31 20:50:27'),
(11, 5, 7, 0, 0, '2021-05-31 20:50:45'),
(12, 5, 7, 0, 0, '2021-05-31 20:51:13'),
(13, 5, 7, 0, 0, '2021-05-31 20:51:37'),
(14, 5, 8, 0, 0, '2021-05-31 20:51:56'),
(15, 5, 9, 0, 0, '2021-05-31 20:52:26'),
(16, 5, 9, 0, 0, '2021-05-31 20:52:48'),
(17, 8, 15, 0, 0, '2021-06-08 17:29:40'),
(18, 8, 15, 0, 0, '2021-06-08 17:29:52'),
(19, 8, 15, 0, 0, '2021-06-08 17:30:31'),
(20, 18, 28, 0, 0, '2021-06-29 15:04:17'),
(21, 18, 28, 0, 0, '2021-06-29 15:06:17'),
(22, 16, 25, 0, 0, '2021-06-29 18:37:21'),
(23, 34, 48, 0, 0, '2021-07-06 16:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `betting_subtitle`
--

CREATE TABLE `betting_subtitle` (
  `id` int(12) NOT NULL,
  `title` varchar(100) NOT NULL,
  `bettingId` varchar(255) NOT NULL,
  `showStatus` int(11) NOT NULL DEFAULT 0,
  `limitedAmount` double NOT NULL DEFAULT 0,
  `bettedAmount` double NOT NULL DEFAULT 0,
  `hide` int(11) NOT NULL DEFAULT 0,
  `close` int(11) NOT NULL DEFAULT 0,
  `ariaHide` int(11) NOT NULL DEFAULT 1,
  `waittingTime` int(11) NOT NULL DEFAULT 1,
  `section_ct` int(11) NOT NULL DEFAULT 0,
  `section_hide` int(11) NOT NULL DEFAULT 1,
  `end_date` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_subtitle`
--

INSERT INTO `betting_subtitle` (`id`, `title`, `bettingId`, `showStatus`, `limitedAmount`, `bettedAmount`, `hide`, `close`, `ariaHide`, `waittingTime`, `section_ct`, `section_hide`, `end_date`, `end_time`) VALUES
(1, 'what is the price', '1', 0, 0, 0, 0, 0, 1, 2, 0, 1, NULL, NULL),
(2, 'Wolfsberger AC', '1', 0, 0, 0, 0, 0, 0, 1, 0, 1, NULL, NULL),
(3, 'Austria Wien', '1', 0, 0, 0, 0, 0, 0, 1, 0, 1, NULL, NULL),
(4, 'Kumilla Won?', '2', 0, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(5, 'Chittagong Won?', '2', 0, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(6, 'Rahim', '5', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(7, 'karim', '5', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(8, 'Half time score', '5', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(9, 'full time score', '5', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(10, 'Full Time', '7', 1, 0, 0, 1, 1, 1, 1, 0, 1, NULL, NULL),
(11, 'Half Time', '7', 1, 0, 0, 1, 0, 1, 45, 0, 1, NULL, NULL),
(12, 'Total Goal', '7', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(13, 'To Win The match ', '5', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(14, 'hello', '5', 0, 0, 0, 0, 1, 1, 1, 0, 1, NULL, NULL),
(15, 'Full Time Result ', '8', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(16, 'Half Time Result ', '8', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(17, 'Total Goal ', '8', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(18, 'To Win The Toss ', '9', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(19, 'To Win The Match', '9', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(20, 'To win The match', '10', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(21, 'To win the match', '11', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(22, 'To Win the match', '12', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(23, 'To win the match', '13', 0, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(24, 'To Win The Match....? ', '16', 1, 0, 0, 1, 0, 0, 1, 0, 1, NULL, NULL),
(25, '1st Set Win ', '16', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(26, '1st Set Point', '16', 1, 0, 0, 1, 0, 0, 1, 0, 1, NULL, NULL),
(27, 'Total Match Point ', '16', 0, 0, 0, 0, 1, 1, 1, 0, 1, NULL, NULL),
(28, 'To Win The Toss', '18', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(29, 'To Win The Match ', '18', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(30, 'Full Time Result ', '19', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(31, 'Half Time Result', '19', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(32, 'Total Goal ', '19', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL),
(33, 'To Win The Match ', '20', 1, 0, 0, 1, 0, 0, 1, 0, 1, NULL, NULL),
(34, 'To Win The Match', '21', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(35, 'Full Time Result', '22', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(36, 'To Win The Toss ', '23', 1, 0, 0, 1, 0, 0, 1, 0, 1, NULL, NULL),
(37, 'To Win The Match', '23', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(38, 'Full Time Result ', '25', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(39, 'Half Time Result ', '25', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(40, 'Total Goal ', '25', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(41, '1st Team To Score', '25', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(42, 'Both Team To Score ', '25', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(43, 'To Win The Toss', '26', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(44, 'To Win The Match', '26', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(45, 'To Win The Match ', '31', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(46, 'To Win The Match', '34', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(47, '1st Set Total Points', '34', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(48, '1st Set Winners', '34', 0, 0, 0, 0, 0, 0, 1, 0, 1, NULL, NULL),
(49, 'Total Points', '34', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(50, 'To Win The Match', '36', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(51, 'Total Points ', '36', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(52, 'Full Time Result ', '28', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(53, 'Half Time Result ', '28', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(54, 'Total Goal ', '28', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(55, '1st Team To Score', '28', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(56, 'Both Team To Score', '28', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(57, 'To Win The Match', '33', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(58, 'To Win The Toss ', '29', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(59, 'To Win The Match ', '29', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(60, 'To Win The Toss ', '30', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(61, 'To Win The Match ', '30', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(62, 'To Win The Match ', '37', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(63, 'Total Points ', '37', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(64, 'To Win The Match ', '35', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(65, '1st Set Total Points ', '35', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(66, '1st Set Winners ', '35', 1, 0, 0, 1, 0, 1, 1, 0, 1, NULL, NULL),
(67, 'Total Points ', '35', 0, 0, 0, 0, 0, 1, 1, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `betting_sub_title_option`
--

CREATE TABLE `betting_sub_title_option` (
  `id` int(12) NOT NULL,
  `title` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `bettingSubTitleId` int(11) NOT NULL DEFAULT 0,
  `betCount` int(11) NOT NULL DEFAULT 1,
  `showStatus` int(11) NOT NULL DEFAULT 1,
  `limitedAmount` double NOT NULL DEFAULT 0,
  `bettedAmount` double NOT NULL DEFAULT 0,
  `hide` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_sub_title_option`
--

INSERT INTO `betting_sub_title_option` (`id`, `title`, `amount`, `bettingSubTitleId`, `betCount`, `showStatus`, `limitedAmount`, `bettedAmount`, `hide`) VALUES
(2, 'Manchester City', '1.25', 1, 1, 1, 2, 0, 1),
(3, 'Wolfsberger AC', '1.25', 2, 1, 1, 0, 0, 1),
(4, 'Austria Wien', '2.0', 2, 1, 1, 0, 0, 1),
(5, '1 goal', '1.25', 6, 1, 1, 0, 0, 1),
(9, '2 goal', '2', 7, 1, 1, 0, 0, 1),
(10, '1-1', '1', 8, 1, 1, 0, 0, 1),
(11, '2-2', '2', 9, 1, 1, 0, 0, 1),
(12, '0 goal', '2', 6, 1, 1, 0, 0, 1),
(13, '0 goal', '1.5', 7, 1, 1, 0, 0, 1),
(14, '1 goal', '2.5', 7, 1, 1, 0, 0, 1),
(15, '2.5 over', '1.25', 9, 1, 1, 0, 0, 1),
(16, 'England', '1.50', 10, 1, 1, 0, 0, 1),
(17, 'Draw', '3.60', 10, 1, 1, 0, 0, 1),
(18, 'perague', '5.00', 10, 1, 1, 0, 0, 1),
(19, 'England', '1.90', 11, 1, 1, 0, 0, 1),
(20, 'Draw', '2.00', 11, 1, 1, 0, 0, 1),
(21, 'parague', '5.20', 11, 1, 1, 0, 0, 1),
(22, '2.5 over  ', '1.72', 12, 1, 1, 0, 0, 1),
(23, '2.50 Under', '1.90', 12, 1, 1, 0, 0, 1),
(24, 'Other ', '1.00', 13, 1, 1, 0, 0, 1),
(25, 'Other ', '2.50', 13, 1, 1, 0, 0, 1),
(26, 'over ', '5.90', 13, 1, 1, 0, 0, 1),
(27, 'bangladesh', '2.00', 15, 1, 1, 0, 0, 1),
(28, 'draw', '3.00', 15, 1, 1, 0, 0, 1),
(29, ' india', '4.00', 15, 1, 1, 0, 0, 1),
(30, 'bangladesh', '2.50', 16, 1, 1, 0, 0, 1),
(31, 'india', '3.50', 16, 1, 1, 0, 0, 1),
(32, 'Netherlands', '1.90', 18, 1, 1, 0, 0, 1),
(33, ' Ireland ', '1.90', 18, 1, 1, 0, 0, 1),
(34, 'Netherlands', '2.50', 19, 1, 1, 0, 0, 1),
(35, ' Ireland', '1.30', 19, 1, 1, 0, 0, 1),
(36, 'A', '1.00', 20, 1, 1, 0, 0, 1),
(37, 'B', '1.05', 20, 1, 1, 0, 0, 1),
(38, 'A', '2.00', 21, 1, 1, 0, 0, 1),
(39, 'B', '3.00', 21, 1, 1, 0, 0, 1),
(40, 'A', '1.00', 22, 1, 1, 0, 0, 1),
(41, 'B', '1.00', 22, 1, 1, 0, 0, 1),
(42, 'bangladesh', '1.60', 23, 1, 1, 0, 0, 1),
(43, 'india', '2.30', 23, 1, 1, 0, 0, 1),
(44, ' Tsvetkov E ', '1.80', 24, 1, 1, 0, 0, 1),
(45, ' Sinkovskiy V', '1.80', 24, 1, 1, 0, 0, 1),
(46, ' Tsvetkov E', '2.00', 25, 1, 1, 0, 0, 1),
(47, ' Sinkovskiy V', '1.70', 25, 1, 1, 0, 0, 1),
(48, '18.5 Over', '1.90', 26, 1, 1, 0, 0, 1),
(49, '18.5 Under', '1.90', 26, 1, 1, 0, 0, 1),
(50, 'England', '1.90', 28, 1, 1, 0, 0, 1),
(51, 'Sri Lanka', '1.90', 28, 1, 1, 0, 0, 1),
(52, 'England', '1.66', 29, 1, 1, 0, 0, 1),
(53, 'Sri Lanka', '2.20', 29, 1, 1, 0, 0, 1),
(54, 'England', '2.20', 30, 1, 1, 0, 0, 1),
(55, 'Draw', '3.20', 30, 1, 1, 0, 0, 1),
(56, ' Germany', '2.50', 30, 1, 1, 0, 0, 1),
(57, 'Kovalchuk N', '1.50', 33, 1, 1, 0, 0, 1),
(58, 'Hordii A', '2.40', 33, 1, 1, 0, 0, 1),
(59, ' Hryhoriev O', '2.30', 34, 1, 1, 0, 0, 1),
(60, 'Gatsenko I', '1.60', 34, 1, 1, 0, 0, 1),
(61, 'Newcastle Olympic FC', '2.20', 35, 1, 1, 0, 0, 1),
(62, 'Draw', '3.30', 35, 1, 1, 0, 0, 1),
(63, 'Broadmeadow Magic FC', '2.70', 35, 1, 1, 0, 0, 1),
(64, ' FC Haka ', '1.80', 36, 1, 1, 0, 0, 1),
(65, ' FC Honka', '1.95', 36, 1, 1, 0, 0, 1),
(66, ' FC Haka', '1.95', 37, 1, 1, 0, 0, 1),
(67, ' FC Honka', '1.95', 37, 1, 1, 0, 0, 1),
(68, 'Mons Calpe', '2.30', 38, 1, 1, 0, 0, 1),
(69, 'Draw', '3.30', 38, 1, 1, 0, 0, 1),
(70, 'FC Santa Coloma', '2.50', 38, 1, 1, 0, 0, 1),
(71, 'Mons Calpe', '2.40', 39, 1, 1, 0, 0, 1),
(72, 'Draw', '1.80', 39, 1, 1, 0, 0, 1),
(73, 'FC Santa Coloma', '2.70', 39, 1, 1, 0, 0, 1),
(74, '2.5 Over', '1.75', 40, 1, 1, 0, 0, 1),
(75, '2.5 Under', '1.87', 40, 1, 1, 0, 0, 1),
(76, 'Mons Calpe', '1.80', 41, 1, 1, 0, 0, 1),
(77, 'No Goal', '5.50', 41, 1, 1, 0, 0, 1),
(78, 'FC Santa Coloma', '2.10', 41, 1, 1, 0, 0, 1),
(79, 'yes', '1.66', 42, 1, 1, 0, 0, 1),
(80, 'No', '2.10', 42, 1, 1, 0, 0, 1),
(81, 'Zimbabwe', '1.91', 43, 1, 1, 0, 0, 1),
(82, 'Bangladesh', '1.91', 43, 1, 1, 0, 0, 1),
(83, 'Zimbabwe', '2.10', 44, 1, 1, 0, 0, 1),
(84, 'Bangladesh', '1.72', 44, 1, 1, 0, 0, 1),
(85, 'Saudi Arabia', '2.50', 45, 1, 1, 0, 0, 1),
(86, 'Algeria', '1.40', 45, 1, 1, 0, 0, 1),
(87, '75.5 Over', '1.81', 49, 1, 1, 0, 0, 1),
(88, '75.5 Under', '1.81', 49, 1, 1, 0, 0, 1),
(89, 'Kuzyshyn O', '1.81', 48, 1, 1, 0, 0, 1),
(90, 'Skorobahatyi Y', '1.81', 48, 1, 1, 0, 0, 1),
(91, 'Kuzyshyn O', '1.80', 46, 1, 1, 0, 0, 1),
(92, 'Skorobahatyi Y', '2.00', 46, 1, 1, 0, 0, 1),
(93, '18.5 Over', '1.90', 47, 1, 1, 0, 0, 1),
(94, '18.5 Under', '1.90', 47, 1, 1, 0, 0, 1),
(95, 'Shkendija Tetovo', '1.72', 50, 1, 1, 0, 0, 1),
(96, ' NS Mura', '2.10', 50, 1, 1, 0, 0, 1),
(97, '55.5 Over', '1.70', 51, 1, 1, 0, 0, 1),
(98, '55.5 Under', '2.20', 51, 1, 1, 0, 0, 1),
(99, 'Sanat Naft Abadan', '1.90', 52, 1, 1, 0, 0, 1),
(100, 'Draw', '3.40', 52, 1, 1, 0, 0, 1),
(101, 'Nassaji Mazandaran', '2.60', 52, 1, 1, 0, 0, 1),
(102, 'Sanat Naft Abadan', '2.10', 53, 1, 1, 0, 0, 1),
(103, 'Draw', '2.00', 53, 1, 1, 0, 0, 1),
(104, 'Nassaji Mazandaran', '3.10', 53, 1, 1, 0, 0, 1),
(105, '2.5 Over', '2.00', 54, 1, 1, 0, 0, 1),
(106, '2.5 Under', '1.80', 54, 1, 1, 0, 0, 1),
(107, 'Sanat Naft Abadan ', '1.80', 55, 1, 1, 0, 0, 1),
(108, 'No Goal', '5.80', 55, 1, 1, 0, 0, 1),
(109, ' Nassaji Mazandaran', '2.00', 55, 1, 1, 0, 0, 1),
(110, 'Yes', '2.10', 56, 1, 1, 0, 0, 1),
(111, 'No', '1.70', 56, 1, 1, 0, 0, 1),
(112, 'Foolad Khuzesta', '2.10', 57, 1, 1, 0, 0, 1),
(113, 'Persepolis', '1.80', 57, 1, 1, 0, 0, 1),
(114, 'Pakistan Women', '1.91', 58, 1, 1, 0, 0, 1),
(115, 'West Indies Women', '1.91', 58, 1, 1, 0, 0, 1),
(116, 'Pakistan Women', '1.66', 59, 1, 1, 0, 0, 1),
(117, 'West Indies Women', '2.30', 59, 1, 1, 0, 0, 1),
(118, 'South Africa', '1.91', 60, 1, 1, 0, 0, 1),
(119, 'Ireland', '1.91', 60, 1, 1, 0, 0, 1),
(120, 'South Africa', '1.40', 61, 1, 1, 0, 0, 1),
(121, 'Ireland', '2.50', 61, 1, 1, 0, 0, 1),
(122, 'England', '1.80', 62, 1, 1, 0, 0, 1),
(123, 'Denmark', '2.00', 62, 1, 1, 0, 0, 1),
(124, '45.5 over', '1.90', 63, 1, 1, 0, 0, 1),
(125, '45.5 Under', '1.90', 63, 1, 1, 0, 0, 1),
(126, 'Skorobahatyi Y', '2.10', 64, 1, 1, 0, 0, 1),
(127, 'Sachuk S', '1.72', 64, 1, 1, 0, 0, 1),
(128, 'Skorobahatyi Y', '1.90', 66, 1, 1, 0, 0, 1),
(129, 'Sachuk S', '1.90', 66, 1, 1, 0, 0, 1),
(130, '18.5 Over', '1.85', 65, 1, 1, 0, 0, 1),
(131, '18.5 under', '1.85', 65, 1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `betting_title`
--

CREATE TABLE `betting_title` (
  `id` int(11) NOT NULL,
  `A_team` varchar(50) DEFAULT NULL,
  `B_team` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `gameType` int(11) DEFAULT 0,
  `showStatus` int(11) DEFAULT 0,
  `limitedAmount` double DEFAULT 0,
  `bettedAmount` double DEFAULT 0,
  `hide` int(11) DEFAULT 0,
  `close` int(11) DEFAULT 0,
  `ariaHide` int(11) DEFAULT 1,
  `waittingTime` int(11) DEFAULT 1,
  `time` timestamp NULL DEFAULT current_timestamp(),
  `hideFromPanel` int(11) DEFAULT 1,
  `user` varchar(255) DEFAULT '0',
  `close_time` varchar(50) DEFAULT NULL,
  `color_a` text DEFAULT NULL,
  `color_b` text DEFAULT NULL,
  `cancel` int(12) DEFAULT 0,
  `liveScore` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_title`
--

INSERT INTO `betting_title` (`id`, `A_team`, `B_team`, `title`, `date`, `status`, `gameType`, `showStatus`, `limitedAmount`, `bettedAmount`, `hide`, `close`, `ariaHide`, `waittingTime`, `time`, `hideFromPanel`, `user`, `close_time`, `color_a`, `color_b`, `cancel`, `liveScore`) VALUES
(1, 'Wolfsberger AC', 'Austria Wien', 'AUSTRIA. BUNDESLIGA', '30 May 2021 02:15 AM', 1, 1, 0, 0, 0, 0, 1, 0, 1, '2021-05-30 15:26:03', 1, 'admin', '06 Jun 2021 12:01 AM', 'blue', 'red', 0, '0'),
(2, 'Kumilla', 'Chittagong', 'Who will won the match', '31/05/2021', 1, 1, 1, 0, 0, 1, 1, 1, 20, '2021-05-30 19:54:39', 1, 'admin', '31 May 2021 2:03 AM', 'Red', 'green', 0, '230'),
(3, 'Mirpur', 'Gulshan', 'Sher-e-bangla cricket stadium', '01/06/2021', 1, 1, 0, 0, 0, 1, 1, 1, 10, '2021-05-31 19:09:03', 1, 'admin', '01 Jun 2021 1:22 AM', 'Green', 'Yellow', 0, '1-0'),
(4, 'Mirpur', 'Gulshan', 'Half Time', '01/06/2021', 1, 1, 0, 0, 0, 1, 1, 1, 1, '2021-05-31 19:12:20', 1, 'admin', '01 Jun 2021 1:22 AM', 'Green', 'Yellow', 0, '1'),
(5, 'Rahim', 'Karim', 'national stadium', '01/06/20201', 1, 1, 0, 0, 0, 0, 1, 0, 1, '2021-05-31 19:38:30', 1, 'admin', '05 Jun 2021 11:59 PM', 'Orange', 'green', 0, '0'),
(6, 'TeamA', 'Team B', 'chittagong', '01/06/2021', 1, 1, 0, 0, 0, 0, 1, 1, 1, '2021-05-31 19:41:56', 1, 'admin', '05 Jun 2021 11:59 PM', 'Orange', 'green', 0, '2-0'),
(7, 'England', 'perague', 'Dhaka Stadium', '03/06/20201', 1, 1, 1, 0, 0, 1, 1, 1, 1, '2021-06-02 19:02:43', 1, 'admin', '03 Jun 2021 1:27 AM', 'Black', 'blue', 0, NULL),
(8, 'bangladesh', 'india', 'test', '06 Jun 2021 01:00 am', 1, 1, 0, 0, 0, 0, 1, 0, 1, '2021-06-05 18:49:09', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, 'will live 0-0'),
(9, 'Netherlands', 'Ireland', '3rd ODI', '07 Jun 2021 02:30 pm', 2, 2, 0, 0, 0, 0, 1, 0, 1, '2021-06-05 19:48:47', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, NULL),
(10, 'Abc', 'Cdf', 'Badminton', '06 Jun 2021 08:00 am', 1, 7, 0, 0, 0, 0, 1, 0, 1, '2021-06-05 21:25:01', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, NULL),
(11, 'Sdh', 'fhk', 'Table tennis', '06 Jun 2021 08:00 am', 2, 6, 0, 0, 0, 0, 1, 0, 1, '2021-06-05 21:27:09', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, NULL),
(12, 'A', 'B', 'tennis', '06 Jun 2021 5:00 am', 1, 5, 0, 0, 0, 0, 1, 0, 1, '2021-06-05 21:35:09', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, NULL),
(13, '  bangladesh', ' india', 'basketball', '06 Jun 2021 01:00 am', 2, 3, 0, 0, 0, 0, 1, 0, 1, '2021-06-06 10:45:15', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, NULL),
(14, 'fhjjgsjjshghjjj', 'guioooo', 'jjjkkk', '15jun 2021 8.00 pm', 1, 2, 0, 0, 0, 0, 1, 1, 1, '2021-06-28 20:21:06', 1, 'admin', '29 Jun 2021 5:44 AM', '', '', 0, 'Toss '),
(15, ' Tsvetkov E', 'Sinkovskiy V', 'Russia : Liga Pro Men', '29 Jun 2021 09.00 am', 1, 6, 0, 0, 0, 0, 1, 1, 1, '2021-06-28 23:47:34', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, NULL),
(16, 'Tsvetkov E', 'Sinkovskiy V', 'Russia : Liga Pro Men', '29 Jun 2021 09.00 am', 1, 6, 1, 0, 0, 1, 1, 0, 1, '2021-06-28 23:49:37', 1, 'admin', '03 Jul 2021 1:03 AM', '', '', 0, NULL),
(17, 'England', 'Sri Lanka', 'Sri Lanka tour of England,1st ODI', '29 Jun 2021 04.00 pm', 1, 2, 0, 0, 0, 0, 1, 1, 1, '2021-06-28 23:58:54', 1, 'admin', '29 Jun 2021 6:01 AM', '', '', 0, NULL),
(18, 'England', 'Sri Lanka', '1st ODI', '29 Jun 2021 04.00 pm', 1, 2, 0, 0, 0, 1, 1, 0, 1, '2021-06-29 00:02:47', 1, 'admin', '03 Jul 2021 1:03 AM', '', '', 0, NULL),
(19, 'England', 'Germany', 'Euro 2020', '29 Jun 2021 10:00 pm', 1, 1, 0, 0, 0, 0, 1, 0, 1, '2021-06-29 00:06:53', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, '0-0'),
(20, 'Kovalchuk N ', ' Hordii A', 'TT CUP,Basketball', '29 Jun 2021 12:25pm', 1, 3, 1, 0, 0, 1, 1, 1, 1, '2021-06-29 00:12:24', 1, 'admin', '06 Jul 2021 8:15 PM', '', '', 0, NULL),
(21, 'Hryhoriev O', 'Gatsenko I', 'Setka Cup Badminton', '29 Jun 2021. 06:45pm', 1, 7, 0, 0, 0, 0, 1, 0, 1, '2021-06-29 00:15:34', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, NULL),
(22, 'Newcastle Olympic FC', 'Broadmeadow Magic FC', 'Australia NPL, Northern', 'Jun-30  03:00 PM', 2, 1, 0, 0, 0, 0, 1, 0, 1, '2021-06-29 18:21:56', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, NULL),
(23, 'FC Haka', 'FC Honka', 'Finland Veikkausliiga', 'Jun-30  09:30 PM', 2, 2, 0, 0, 0, 0, 1, 0, 1, '2021-06-29 18:27:07', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, NULL),
(24, 'Bangladesh Police Club', 'Brothers Union', 'Bangladesh Premier League', 'Jun-30 2021 || 06:45 PM', 2, 2, 0, 0, 0, 0, 1, 1, 1, '2021-06-29 18:32:16', 1, 'admin', '06 Jul 2021 8:16 PM', '', '', 0, NULL),
(25, 'Mons Calpe', 'FC Santa Coloma', 'Europe UEFA Champions League, Qualification', '06 Jul 09:50 pm', 1, 1, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:19:33', 1, 'admin', NULL, '', '', 0, NULL),
(26, 'Zimbabwe ', 'Bangladesh', 'Only Test', '06 Jul 10:00 pm', 1, 2, 1, 0, 0, 1, 0, 1, 1, '2021-07-06 14:24:50', 1, 'admin', NULL, '', '', 0, NULL),
(27, 'Sanat Naft Abadan', 'Nassaji Mazandaran', 'Iran Persian Gulf Pro League', ' 06 Jul 10:30 pm', 2, 1, 0, 0, 0, 0, 1, 1, 1, '2021-07-06 14:27:57', 1, 'admin', '06 Jul 2021 8:30 PM', '', '', 0, NULL),
(28, 'Sanat Naft Abadan', 'Nassaji Mazandaran', 'Iran Persian Gulf Pro League ', '06 Jul 10:30pm', 2, 1, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:29:29', 1, 'admin', NULL, '', '', 0, NULL),
(29, 'Pakistan Women', 'West Indies Women', '1st ODI', '07 Jul 07:30pm', 2, 2, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:32:18', 1, 'admin', NULL, '', '', 0, NULL),
(30, 'South Africa', 'Ireland', '1st ODI', '11 Jul 03:30 pm', 2, 2, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:34:04', 1, 'admin', NULL, '', '', 0, NULL),
(31, 'Saudi Arabia', 'Algeria', 'World U20 Arab Basketball', '06 Jul 10:30 pm', 1, 3, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:36:03', 1, 'admin', NULL, '', '', 0, NULL),
(32, 'Foolad Khuzestan', 'Persepolis', 'Persian Gulf Pro Leagu Basketball', ' 06 Jul 11:30 pm', 2, 3, 0, 0, 0, 0, 1, 1, 1, '2021-07-06 14:54:23', 1, 'admin', '06 Jul 2021 8:57 PM', '', '', 0, NULL),
(33, 'Foolad Khuzestan', 'Persepolis', 'Persian Gulf Pro Leagu Basketball', '06 Jul 11:30 pm', 1, 3, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:57:18', 1, 'admin', NULL, '', '', 0, NULL),
(34, 'Kuzyshyn O', 'Skorobahatyi Y', 'Setka Cup Table Tennis', '06 Jul 11:30 pm', 1, 6, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 14:59:48', 1, 'admin', NULL, '', '', 0, NULL),
(35, 'Skorobahatyi Y', 'Sachuk S', 'Setka Cup Table Tennis', '07 Jul 08:00 pm', 2, 6, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 15:01:18', 1, 'admin', NULL, '', '', 0, NULL),
(36, 'Shkendija Tetovo', 'NS Mura', 'UEFA Champions  Badminton', '06 Jul 11:50 pm', 1, 7, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 15:04:31', 1, 'admin', NULL, '', '', 0, NULL),
(37, 'England', 'Denmark', 'Euro 2020 Badminton', '10 Jul 05:30 pm', 2, 7, 1, 0, 0, 1, 0, 0, 1, '2021-07-06 15:07:20', 1, 'admin', NULL, '', '', 0, NULL),
(38, 'kuddus', 'fakir', 'hdndnd', '07/07/2012', 1, 1, 0, 0, 0, 0, 1, 1, 1, '2021-07-06 17:14:30', 1, 'admin', '06 Jul 2021 11:15 PM', '', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bettintransaction`
--

CREATE TABLE `bettintransaction` (
  `id` int(12) NOT NULL,
  `totalSending` varchar(255) NOT NULL DEFAULT '0',
  `totalGaining` varchar(255) NOT NULL DEFAULT '0',
  `totalSaving` varchar(255) NOT NULL DEFAULT '0',
  `s_save` double NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet_history`
--

CREATE TABLE `bet_history` (
  `id` int(12) NOT NULL,
  `question` text NOT NULL,
  `match` text NOT NULL,
  `credit` double NOT NULL DEFAULT 1,
  `debit` double NOT NULL DEFAULT 1,
  `balance` double NOT NULL DEFAULT 1,
  `userBalance` double NOT NULL DEFAULT 0,
  `time` varchar(50) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bet_history`
--

INSERT INTO `bet_history` (`id`, `question`, `match`, `credit`, `debit`, `balance`, `userBalance`, `time`) VALUES
(1, 'what is the price', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:12 PM'),
(2, 'what is the price', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:13 PM'),
(3, 'what is the price', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:32 PM'),
(4, 'Wolfsberger AC', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:43 PM'),
(5, 'Wolfsberger AC', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:43 PM'),
(6, 'Wolfsberger AC', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:43 PM'),
(7, 'Wolfsberger AC', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 1, 0, 0, 105000, '30 May 2021 10:45 PM'),
(8, 'Wolfsberger AC', 'Wolfsberger AC VS Austria Wien 30 May 2021 02:15 AM', 900, 1, 900, 103900, '30 May 2021 11:37 PM'),
(9, 'Kumilla Won?', 'Kumilla VS Chittagong 31/05/2021 (mistake)', 1, 0, 0, 107900, '31 May 2021 1:56 AM'),
(10, 'Kumilla Won?', 'Kumilla VS Chittagong 31/05/2021 (mistake)', 1, 0, 0, 107900, '31 May 2021 2:01 AM'),
(11, 'Rahim', 'Rahim VS Karim 01/06/20201', 87.5, 1, 87.5, 156050, '01 Jun 2021 2:50 AM'),
(12, 'Rahim', 'Rahim VS Karim 01/06/20201', 1, -200, -200, 156112.5, '01 Jun 2021 2:50 AM'),
(13, 'karim', 'Rahim VS Karim 01/06/20201', 200, 1, 200, 156112.5, '01 Jun 2021 2:50 AM'),
(14, 'karim', 'Rahim VS Karim 01/06/20201', 1, -150, -150, 156312.5, '01 Jun 2021 2:51 AM'),
(15, 'karim', 'Rahim VS Karim 01/06/20201', 1, -500, -500, 156312.5, '01 Jun 2021 2:51 AM'),
(16, 'Half time score', 'Rahim VS Karim 01/06/20201', 1, 0, 0, 156312.5, '01 Jun 2021 2:51 AM'),
(17, 'full time score', 'Rahim VS Karim 01/06/20201', 1, -25, -25, 156412.5, '01 Jun 2021 2:52 AM'),
(18, 'full time score', 'Rahim VS Karim 01/06/20201', 1, 0, 0, 156537.5, '01 Jun 2021 2:52 AM'),
(19, 'Full Time Result ', 'bangladesh VS india 06 Jun 2021 01:00 am', 4500, 1, 4500, 257578.5, '08 Jun 2021 11:29 PM'),
(20, 'Full Time Result ', 'bangladesh VS india 06 Jun 2021 01:00 am', 1, -10200, -10200, 258478.5, '08 Jun 2021 11:29 PM'),
(21, 'Full Time Result ', 'bangladesh VS india 06 Jun 2021 01:00 am (mistake)', 1, 4500, -4500, 257578.5, '08 Jun 2021 11:30 PM'),
(22, 'Full Time Result ', 'bangladesh VS india 06 Jun 2021 01:00 am', 1, -4800, -4800, 257578.5, '08 Jun 2021 11:30 PM'),
(23, 'Full Time Result ', 'bangladesh VS india 06 Jun 2021 01:00 am (mistake)', 1, -4800, 4800, 257578.5, '08 Jun 2021 11:30 PM'),
(24, 'To Win The Toss', 'England VS Sri Lanka 29 Jun 2021 04.00 pm', 1, 0, 0, 257278.5, '29 Jun 2021 9:04 PM'),
(25, 'To Win The Toss', 'England VS Sri Lanka 29 Jun 2021 04.00 pm', 1, -90, -90, 257178.5, '29 Jun 2021 9:06 PM'),
(26, 'To Win The Toss', 'England VS Sri Lanka 29 Jun 2021 04.00 pm (mistake)', 1, -90, 90, 257178.5, '29 Jun 2021 9:07 PM'),
(27, '1st Set Win ', 'Tsvetkov E VS Sinkovskiy V 29 Jun 2021 09.00 am', 1, -500, -500, 251678.5, '30 Jun 2021 12:37 AM'),
(28, '1st Set Winners', 'Kuzyshyn O VS Skorobahatyi Y 06 Jul 11:30 pm', 1, -810, -810, 52321, '06 Jul 2021 10:45 PM');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(12) NOT NULL,
  `userId` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  `admin` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `userId`, `msg`, `admin`, `time`) VALUES
(1, 'ksajib', 'hi how r u?', 1, '2021-05-30 20:05:23');

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` int(12) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `balance` double NOT NULL DEFAULT 0,
  `rate` double NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `personalIdOfClub` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`id`, `name`, `userId`, `password`, `mobileNumber`, `email`, `balance`, `rate`, `active`, `personalIdOfClub`) VALUES
(1, 'ksajib', 'ksajib', 'ksajib', '0191245789', 'sajib@gmail.com', 40313.3, 2, 1, '001'),
(2, 'ayush', 'ayush', '123456', '01971980980', 'ayush@gmail.com', 0, 2, 1, '0'),
(3, 'ashik', 'ashik99', '123456', '01999999999', 'ashik@gmail.com', 0, 2, 1, 'sky');

-- --------------------------------------------------------

--
-- Table structure for table `default_ans`
--

CREATE TABLE `default_ans` (
  `id` int(12) NOT NULL,
  `title` varchar(100) NOT NULL,
  `bettingSubTitleId` int(11) NOT NULL DEFAULT 0,
  `amount` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_match`
--

CREATE TABLE `default_match` (
  `id` int(12) NOT NULL,
  `A_team` varchar(100) NOT NULL,
  `B_team` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `date` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `gameType` int(11) NOT NULL DEFAULT 0,
  `ariaHide` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_ques`
--

CREATE TABLE `default_ques` (
  `id` int(12) NOT NULL,
  `title` varchar(100) NOT NULL,
  `bettingId` varchar(255) NOT NULL,
  `ariaHide` int(11) NOT NULL DEFAULT 1,
  `section_ct` int(11) NOT NULL DEFAULT 1,
  `g_type` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deposit_and_withdraw_his`
--

CREATE TABLE `deposit_and_withdraw_his` (
  `id` int(12) NOT NULL,
  `m_number` varchar(50) NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `startDate` varchar(50) NOT NULL,
  `finishDate` varchar(50) NOT NULL,
  `d_or_w` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_and_withdraw_his`
--

INSERT INTO `deposit_and_withdraw_his` (`id`, `m_number`, `amount`, `startDate`, `finishDate`, `d_or_w`) VALUES
(1, 'ketaedvfdfsdft', 100000, '30 May 2021 9:47 PM', '30 May 2021 9:47 PM', 1),
(2, 'jhgjgjh', 5000, '31 May 2021 1:32 AM', '31 May 2021 1:32 AM', 1),
(3, '', 0, '31 May 2021 2:32 AM', '04 Jun 2021 12:25 AM', 2),
(4, 'kjhkjhk', 58000, '04 Jun 2021 1:16 AM', '04 Jun 2021 1:16 AM', 1),
(5, '1234', 200, '05 Jun 2021 4:34 PM', '05 Jun 2021 4:34 PM', 1),
(6, '12', 200, '06 Jun 2021 1:04 AM', '06 Jun 2021 1:04 AM', 1),
(7, 'THIS FIELD IS HIDDEN', 61000, '06 Jun 2021 8:23 PM', '03 Jul 2021 1:04 AM', 1),
(8, 'Select number', 17422, '07 Jun 2021 12:51 AM', '07 Jul 2021 1:47 AM', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ending_time`
--

CREATE TABLE `ending_time` (
  `id` int(11) NOT NULL,
  `match_id` tinytext DEFAULT NULL,
  `end_time` tinytext DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ending_time`
--

INSERT INTO `ending_time` (`id`, `match_id`, `end_time`, `status`) VALUES
(1, '1', '1622345400', 0),
(2, '2', '1622349660', 0),
(3, '3', '1622349660', 0),
(4, '4', '1622404440', 0),
(5, '5', '1622404440', 0),
(6, '6', '1622492760', 0),
(7, '7', '1622492760', 0),
(8, '8', '1622492760', 0),
(9, '9', '1622492760', 0),
(10, '10', '1622660460', 0),
(11, '11', '1622660460', 0),
(12, '12', '1622660460', 0),
(13, '13', '1622845620', 0),
(14, '14', '943898400', 0),
(15, '15', '943898400', 0),
(16, '16', '943898400', 0),
(17, '17', '943898400', 0),
(18, '18', '943898400', 0),
(19, '19', '943898400', 0),
(20, '20', '943898400', 0),
(21, '21', '943898400', 0),
(22, '22', '943898400', 0),
(23, '23', '943898400', 0),
(24, '24', '943898400', 0),
(25, '25', '943898400', 0),
(26, '26', '943898400', 0),
(27, '27', '943898400', 0),
(28, '28', '943898400', 0),
(29, '29', '943898400', 0),
(30, '30', '943898400', 0),
(31, '31', '943898400', 0),
(32, '32', '943898400', 0),
(33, '33', '943898400', 0),
(34, '34', '943898400', 0),
(35, '35', '943898400', 0),
(36, '36', '943898400', 0),
(37, '37', '943898400', 0),
(38, '38', '943898400', 0),
(39, '39', '943898400', 0),
(40, '40', '943898400', 0),
(41, '41', '943898400', 0),
(42, '42', '943898400', 0),
(43, '43', '943898400', 0),
(44, '44', '943898400', 0),
(45, '45', '943898400', 0),
(46, '46', '943898400', 0),
(47, '47', '943898400', 0),
(48, '48', '943898400', 0),
(49, '49', '943898400', 0),
(50, '50', '943898400', 0),
(51, '51', '943898400', 0),
(52, '52', '943898400', 0),
(53, '53', '943898400', 0),
(54, '54', '943898400', 0),
(55, '55', '943898400', 0),
(56, '56', '943898400', 0),
(57, '57', '943898400', 0),
(58, '58', '943898400', 0),
(59, '59', '943898400', 0),
(60, '60', '943898400', 0),
(61, '61', '943898400', 0),
(62, '62', '943898400', 0),
(63, '63', '943898400', 0),
(64, '64', '943898400', 0),
(65, '65', '943898400', 0),
(66, '66', '943898400', 0),
(67, '67', '943898400', 0);

-- --------------------------------------------------------

--
-- Table structure for table `global_setting`
--

CREATE TABLE `global_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `_key` varchar(30) NOT NULL,
  `value` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `global_setting`
--

INSERT INTO `global_setting` (`id`, `_key`, `value`, `status`, `created_at`) VALUES
(1, 'withdraw', 'withdraw setting', 1, '2021-05-29 10:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `hiddenmatch`
--

CREATE TABLE `hiddenmatch` (
  `id` int(12) NOT NULL,
  `matchId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hiddenmatch`
--

INSERT INTO `hiddenmatch` (`id`, `matchId`, `adminId`) VALUES
(1, 13, 14);

-- --------------------------------------------------------

--
-- Table structure for table `ip`
--

CREATE TABLE `ip` (
  `id` int(12) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `totalVisit` int(11) NOT NULL DEFAULT 0,
  `tag` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iptrack`
--

CREATE TABLE `iptrack` (
  `id` int(12) NOT NULL,
  `ip_address` varchar(455) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ip_permit`
--

CREATE TABLE `ip_permit` (
  `id` int(12) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `tg` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `method`
--

CREATE TABLE `method` (
  `id` int(12) NOT NULL,
  `method` varchar(20) NOT NULL,
  `rate` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `method`
--

INSERT INTO `method` (`id`, `method`, `rate`) VALUES
(3, 'Bkash', 1);

-- --------------------------------------------------------

--
-- Table structure for table `multibet`
--

CREATE TABLE `multibet` (
  `id` int(11) NOT NULL,
  `betBy` varchar(250) NOT NULL,
  `details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `rate` float NOT NULL,
  `amount` int(11) NOT NULL,
  `returnAmount` int(11) NOT NULL,
  `won` int(11) NOT NULL,
  `gameLeft` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `multibethelper`
--

CREATE TABLE `multibethelper` (
  `id` int(11) NOT NULL,
  `ansId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `whoBeted` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(12) NOT NULL,
  `text` text NOT NULL,
  `text2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `text`, `text2`) VALUES
(1, '<p>Welcome To Gamet20.com</p>\r\n', '<p>Hi</p>\r\n'),
(2, '<p>Welcome To Gamet20.com</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) UNSIGNED NOT NULL,
  `content` varchar(255) NOT NULL,
  `game_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `content`, `game_type`) VALUES
(1, 'Corner', 1),
(2, 'Offside', 1),
(3, 'Freekick', 1),
(4, 'Penalty', 1),
(5, 'Extra', 1),
(6, 'Extra', 1),
(7, 'Extra', 2),
(8, 'Extra', 2);

-- --------------------------------------------------------

--
-- Table structure for table `receiving_money_number`
--

CREATE TABLE `receiving_money_number` (
  `id` int(12) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receiving_money_number`
--

INSERT INTO `receiving_money_number` (`id`, `phone`) VALUES
(3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `refresh`
--

CREATE TABLE `refresh` (
  `id` int(11) NOT NULL,
  `refresh` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE `rule` (
  `id` int(11) NOT NULL,
  `minimumBalance` double NOT NULL DEFAULT 0,
  `waitingTime` int(11) NOT NULL DEFAULT 0,
  `waitingTimeAfterDeposit` int(11) NOT NULL DEFAULT 0,
  `clubCommission` double NOT NULL DEFAULT 2,
  `userSponsor` double NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`id`, `minimumBalance`, `waitingTime`, `waitingTimeAfterDeposit`, `clubCommission`, `userSponsor`) VALUES
(1, 10, 0, 10, 2, 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(10) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sending_money_number`
--

CREATE TABLE `sending_money_number` (
  `id` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `deposit` double NOT NULL DEFAULT 0,
  `withdrawal` double NOT NULL DEFAULT 0,
  `debit` double NOT NULL DEFAULT 0,
  `credit` double NOT NULL DEFAULT 0,
  `time` varchar(50) DEFAULT NULL,
  `clubId` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(5000) NOT NULL DEFAULT '0',
  `clubCredit` double NOT NULL DEFAULT 0,
  `clubDebit` double NOT NULL DEFAULT 0,
  `total` double NOT NULL DEFAULT 0,
  `sposor` double NOT NULL DEFAULT 0,
  `num` varchar(20) NOT NULL DEFAULT 'null'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `userId`, `deposit`, `withdrawal`, `debit`, `credit`, `time`, `clubId`, `description`, `clubCredit`, `clubDebit`, `total`, `sposor`, `num`) VALUES
(1, 'test', 0, 0, 0, 100000, '30 May 2021 9:47 PM', '0', 'deposit', 0, 0, 105000, 0, 'null'),
(2, 'test', 0, 0, 2, 0, '30 May 2021 11:35 PM', 'ksajib', 'club commission', 2, 0, 2, 0, 'null'),
(3, 'test', 0, 0, 100, 0, '30 May 2021 11:35 PM', '0', 'Bet Placed', 0, 0, 104900, 0, 'null'),
(4, 'test', 0, 0, 20, 0, '30 May 2021 11:35 PM', 'ksajib', 'club commission', 20, 0, 22, 0, 'null'),
(5, 'test', 0, 0, 1000, 0, '30 May 2021 11:35 PM', '0', 'Bet Placed', 0, 0, 103900, 0, 'null'),
(6, 'test', 0, 0, 0, 200, '30 May 2021 11:37 PM', '0', 'Bet Win', 0, 0, 104100, 0, 'null'),
(7, 'sky', 0, 0, 0, 5000, '31 May 2021 1:32 AM', '0', 'deposit', 0, 0, 5000, 0, 'null'),
(8, 'sky', 0, 0, 1000, 0, '31 May 2021 1:35 AM', '0', 'balance transfer to test', 0, 0, 4000, 0, 'null'),
(9, 'test', 0, 0, 0, 1000, '31 May 2021 1:35 AM', '0', 'balance transfer from sky', 0, 0, 105100, 0, 'null'),
(10, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 24, 0, 'null'),
(11, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3900, 0, 'null'),
(12, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 26, 0, 'null'),
(13, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3800, 0, 'null'),
(14, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 28, 0, 'null'),
(15, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3700, 0, 'null'),
(16, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 30, 0, 'null'),
(17, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 32, 0, 'null'),
(18, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3600, 0, 'null'),
(19, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3500, 0, 'null'),
(20, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 34, 0, 'null'),
(21, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3400, 0, 'null'),
(22, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 36, 0, 'null'),
(23, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3300, 0, 'null'),
(24, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 38, 0, 'null'),
(25, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3200, 0, 'null'),
(26, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 40, 0, 'null'),
(27, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 42, 0, 'null'),
(28, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3100, 0, 'null'),
(29, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 3000, 0, 'null'),
(30, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 44, 0, 'null'),
(31, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 2900, 0, 'null'),
(32, 'sky', 0, 0, 2, 0, '31 May 2021 1:48 AM', 'ksajib', 'club commission', 2, 0, 46, 0, 'null'),
(33, 'sky', 0, 0, 100, 0, '31 May 2021 1:48 AM', '0', 'Bet Placed', 0, 0, 2800, 0, 'null'),
(34, '', 0, 0, 5000, 0, '31 May 2021 2:31 AM', 'ksajib', 'withdraw', 0, 0, 45000, 0, 'null'),
(35, 'Mhasanbdfp', 0, 0, 20, 0, '31 May 2021 2:53 AM', 'ksajib', 'club commission', 20, 0, 45020, 0, 'null'),
(36, 'Mhasanbdfp', 0, 0, 1000, 0, '31 May 2021 2:53 AM', '0', 'Bet Placed', 0, 0, 49000, 0, 'null'),
(37, 'sky', 0, 0, 2, 0, '31 May 2021 1:27 PM', 'ksajib', 'club commission', 2, 0, 45022, 0, 'null'),
(38, 'sky', 0, 0, 100, 0, '31 May 2021 1:27 PM', '0', 'Bet Placed', 0, 0, 2700, 0, 'null'),
(39, 'sky', 0, 0, 1, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 1, 0, 45023, 0, 'null'),
(40, 'sky', 0, 0, 50, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2650, 0, 'null'),
(41, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 2, 0, 45025, 0, 'null'),
(42, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2550, 0, 'null'),
(43, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 2, 0, 45027, 0, 'null'),
(44, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2450, 0, 'null'),
(45, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 2, 0, 45029, 0, 'null'),
(46, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2350, 0, 'null'),
(47, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 2, 0, 45031, 0, 'null'),
(48, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2250, 0, 'null'),
(49, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:43 AM', 'ksajib', 'club commission', 2, 0, 45033, 0, 'null'),
(50, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:43 AM', '0', 'Bet Placed', 0, 0, 2150, 0, 'null'),
(51, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:48 AM', 'ksajib', 'club commission', 2, 0, 45035, 0, 'null'),
(52, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:48 AM', '0', 'Bet Placed', 0, 0, 2050, 0, 'null'),
(53, 'sky', 0, 0, 2, 0, '01 Jun 2021 2:48 AM', 'ksajib', 'club commission', 2, 0, 45037, 0, 'null'),
(54, 'sky', 0, 0, 100, 0, '01 Jun 2021 2:48 AM', '0', 'Bet Placed', 0, 0, 1950, 0, 'null'),
(55, 'sky', 0, 0, 0, 62.5, '01 Jun 2021 2:50 AM', '0', 'Bet Win', 0, 0, 2012.5, 0, 'null'),
(56, 'sky', 0, 0, 0, 200, '01 Jun 2021 2:50 AM', '0', 'Bet Win', 0, 0, 2212.5, 0, 'null'),
(57, 'sky', 0, 0, 0, 100, '01 Jun 2021 2:51 AM', '0', 'Bet Win', 0, 0, 2312.5, 0, 'null'),
(58, 'sky', 0, 0, 0, 125, '01 Jun 2021 2:52 AM', '0', 'Bet Win', 0, 0, 2437.5, 0, 'null'),
(59, 'sky', 0, 0, 1000, 0, '03 Jun 2021 12:57 AM', '0', 'withdraw', 0, 0, 1437.5, 0, 'null'),
(60, 'sky', 0, 0, 0, 1000, '03 Jun 2021 12:59 AM', '0', 'withrawal cancel', 0, 0, 2437.5, 0, 'null'),
(61, 'sky', 0, 0, 4, 0, '03 Jun 2021 1:10 AM', 'ksajib', 'club commission', 4, 0, 45041, 0, 'null'),
(62, 'sky', 0, 0, 200, 0, '03 Jun 2021 1:10 AM', '0', 'Bet Placed', 0, 0, 2237.5, 0, 'null'),
(63, 'test', 0, 0, 1000, 0, '03 Jun 2021 10:02 PM', '0', 'this is a test amount transfer (to sky)', 0, 0, 104100, 0, 'null'),
(64, 'sky', 0, 0, 0, 1000, '03 Jun 2021 10:02 PM', '0', 'this is a test amount transfer  (from test)', 0, 0, 3237.5, 0, 'null'),
(65, 'test', 0, 0, 5000, 0, '04 Jun 2021 12:05 AM', '0', 'bonus (to sky)', 0, 0, 99100, 0, 'null'),
(66, 'sky', 0, 0, 0, 5000, '04 Jun 2021 12:05 AM', '0', 'bonus  (from test)', 0, 0, 8237.5, 0, 'null'),
(67, '', 0, 0, 5000, 0, '04 Jun 2021 12:22 AM', 'ksajib', 'withdraw', 0, 0, 40041, 0, 'null'),
(68, 'shamim123', 0, 0, 0, 58000, '04 Jun 2021 1:16 AM', '0', 'deposit', 0, 0, 58000, 0, 'null'),
(69, 'sky', 0, 0, 0, 200, '05 Jun 2021 4:34 PM', '0', 'deposit', 0, 0, 8437.5, 0, 'null'),
(70, 'sky', 0, 0, 2, 0, '05 Jun 2021 11:57 PM', 'ksajib', 'club commission', 2, 0, 40043, 0, 'null'),
(71, 'sky', 0, 0, 100, 0, '05 Jun 2021 11:57 PM', '0', 'Bet Placed', 0, 0, 8337.5, 0, 'null'),
(72, 'sky', 0, 0, 2000, 0, '06 Jun 2021 12:39 AM', '0', 'withdraw', 0, 0, 6337.5, 0, 'null'),
(73, 'sky', 0, 0, 0, 2000, '06 Jun 2021 12:40 AM', '0', 'withrawal cancel', 0, 0, 8337.5, 0, 'null'),
(74, 'sky', 0, 0, 0, 200, '06 Jun 2021 1:04 AM', '0', 'deposit', 0, 0, 8537.5, 0, 'null'),
(75, 'sky', 0, 0, 10.74, 0, '06 Jun 2021 4:51 PM', 'ksajib', 'club commission', 10.74, 0, 40053.74, 0, 'null'),
(76, 'sky', 0, 0, 537, 0, '06 Jun 2021 4:51 PM', '0', 'Bet Placed', 0, 0, 8000.5, 0, 'null'),
(77, 'sky', 0, 0, 0, 50000, '06 Jun 2021 8:23 PM', '0', 'deposit', 0, 0, 58000.5, 0, 'null'),
(78, 'sky', 0, 0, 2, 0, '06 Jun 2021 8:25 PM', 'ksajib', 'club commission', 2, 0, 40055.74, 0, 'null'),
(79, 'sky', 0, 0, 100, 0, '06 Jun 2021 8:25 PM', '0', 'Bet Placed', 0, 0, 57900.5, 0, 'null'),
(80, 'sky', 0, 0, 2, 0, '06 Jun 2021 8:25 PM', 'ksajib', 'club commission', 2, 0, 40057.74, 0, 'null'),
(81, 'sky', 0, 0, 2, 0, '06 Jun 2021 8:25 PM', 'ksajib', 'club commission', 2, 0, 40059.74, 0, 'null'),
(82, 'sky', 0, 0, 100, 0, '06 Jun 2021 8:25 PM', '0', 'Bet Placed', 0, 0, 57800.5, 0, 'null'),
(83, 'sky', 0, 0, 100, 0, '06 Jun 2021 8:25 PM', '0', 'Bet Placed', 0, 0, 57700.5, 0, 'null'),
(84, 'test', 0, 0, 1000, 0, '07 Jun 2021 12:50 AM', '0', 'withdraw', 0, 0, 98100, 0, 'null'),
(85, 'sky', 0, 0, 4000, 0, '07 Jun 2021 7:39 AM', '0', 'withdraw', 0, 0, 53700.5, 0, 'null'),
(86, 'sky', 0, 0, 0, 5000, '07 Jun 2021 7:39 AM', '0', 'deposit', 0, 0, 58700.5, 0, 'null'),
(87, 'sky', 0, 0, 2, 0, '07 Jun 2021 9:52 AM', 'ksajib', 'club commission', 2, 0, 40061.74, 0, 'null'),
(88, 'sky', 0, 0, 100, 0, '07 Jun 2021 9:52 AM', '0', 'Bet Placed', 0, 0, 58600.5, 0, 'null'),
(89, 'sky', 0, 0, 5000, 0, '07 Jun 2021 10:14 AM', '0', 'withdraw', 0, 0, 53600.5, 0, 'null'),
(90, 'sky', 0, 0, 5000, 0, '07 Jun 2021 10:14 AM', '0', 'withdraw', 0, 0, 48600.5, 0, 'null'),
(91, 'sky', 0, 0, 0, 5000, '07 Jun 2021 10:33 AM', '0', 'deposit', 0, 0, 53600.5, 0, 'null'),
(92, 'sky', 0, 0, 0, 5000, '07 Jun 2021 10:33 AM', '0', 'withrawal cancel', 0, 0, 58600.5, 0, 'null'),
(93, 'sky', 0, 0, 1122, 0, '07 Jun 2021 11:08 PM', '0', 'withdraw', 0, 0, 57478.5, 0, 'null'),
(94, 'sky', 0, 0, 1122, 0, '07 Jun 2021 11:09 PM', '0', 'withdraw', 0, 0, 56356.5, 0, 'null'),
(95, 'sky', 0, 0, 0, 1122, '08 Jun 2021 1:14 AM', '0', 'withrawal cancel', 0, 0, 57478.5, 0, 'null'),
(96, 'sky', 0, 0, 100, 0, '08 Jun 2021 11:28 PM', 'ksajib', 'club commission', 100, 0, 40161.74, 0, 'null'),
(97, 'sky', 0, 0, 5000, 0, '08 Jun 2021 11:28 PM', '0', 'Bet Placed', 0, 0, 52478.5, 0, 'null'),
(98, 'sky', 0, 0, 0, 300, '08 Jun 2021 11:29 PM', '0', 'Bet Win', 0, 0, 52778.5, 0, 'null'),
(99, 'sky', 0, 0, 0, 300, '08 Jun 2021 11:29 PM', '0', 'Bet Win', 0, 0, 53078.5, 0, 'null'),
(100, 'sky', 0, 0, 0, 300, '08 Jun 2021 11:29 PM', '0', 'Bet Win', 0, 0, 53378.5, 0, 'null'),
(101, 'sky', 0, 0, 300, 0, '08 Jun 2021 11:30 PM', '0', 'return from you', 0, 0, 53078.5, 0, 'null'),
(102, 'sky', 0, 0, 300, 0, '08 Jun 2021 11:30 PM', '0', 'return from you', 0, 0, 52778.5, 0, 'null'),
(103, 'sky', 0, 0, 300, 0, '08 Jun 2021 11:30 PM', '0', 'return from you', 0, 0, 52478.5, 0, 'null'),
(104, 'sky', 0, 0, 0, 200, '08 Jun 2021 11:30 PM', '0', 'Bet Win', 0, 0, 52678.5, 0, 'null'),
(105, 'sky', 0, 0, 0, 10000, '08 Jun 2021 11:30 PM', '0', 'Bet Win', 0, 0, 62678.5, 0, 'null'),
(106, 'sky', 0, 0, 200, 0, '08 Jun 2021 11:30 PM', '0', 'return from you', 0, 0, 62478.5, 0, 'null'),
(107, 'sky', 0, 0, 10000, 0, '08 Jun 2021 11:30 PM', '0', 'return from you', 0, 0, 52478.5, 0, 'null'),
(108, 'sky', 0, 0, 2, 0, '28 Jun 2021 1:52 AM', 'ksajib', 'club commission', 2, 0, 40163.74, 0, 'null'),
(109, 'sky', 0, 0, 100, 0, '28 Jun 2021 1:52 AM', '0', 'Bet Placed', 0, 0, 52378.5, 0, 'null'),
(110, 'sky', 0, 0, 2, 0, '28 Jun 2021 3:01 AM', 'ksajib', 'club commission', 2, 0, 40165.74, 0, 'null'),
(111, 'sky', 0, 0, 100, 0, '28 Jun 2021 3:01 AM', '0', 'Bet Placed', 0, 0, 52278.5, 0, 'null'),
(112, 'sky', 0, 0, 2, 0, '29 Jun 2021 4:22 AM', 'ksajib', 'club commission', 2, 0, 40167.74, 0, 'null'),
(113, 'sky', 0, 0, 100, 0, '29 Jun 2021 4:22 AM', '0', 'Bet Placed', 0, 0, 52178.5, 0, 'null'),
(114, 'sky', 0, 0, 2, 0, '29 Jun 2021 9:05 PM', 'ksajib', 'club commission', 2, 0, 40169.74, 0, 'null'),
(115, 'sky', 0, 0, 100, 0, '29 Jun 2021 9:05 PM', '0', 'Bet Placed', 0, 0, 52078.5, 0, 'null'),
(116, 'sky', 0, 0, 0, 190, '29 Jun 2021 9:06 PM', '0', 'Bet Win', 0, 0, 52268.5, 0, 'null'),
(117, 'sky', 0, 0, 190, 0, '29 Jun 2021 9:07 PM', '0', 'return from you', 0, 0, 52078.5, 0, 'null'),
(118, 'sky', 0, 0, 60, 0, '30 Jun 2021 12:36 AM', 'ksajib', 'club commission', 60, 0, 40229.74, 0, 'null'),
(119, 'sky', 0, 0, 3000, 0, '30 Jun 2021 12:36 AM', '0', 'Bet Placed', 0, 0, 49078.5, 0, 'null'),
(120, 'sky', 0, 0, 50, 0, '30 Jun 2021 12:36 AM', 'ksajib', 'club commission', 50, 0, 40279.74, 0, 'null'),
(121, 'sky', 0, 0, 2500, 0, '30 Jun 2021 12:36 AM', '0', 'Bet Placed', 0, 0, 46578.5, 0, 'null'),
(122, 'sky', 0, 0, 0, 6000, '30 Jun 2021 12:37 AM', '0', 'Bet Win', 0, 0, 52578.5, 0, 'null'),
(123, 'sky', 0, 0, 0, 500, '03 Jul 2021 1:04 AM', '0', 'deposit', 0, 0, 53078.5, 0, 'null'),
(124, 'sky', 0, 0, 0, 500, '03 Jul 2021 1:04 AM', '0', 'deposit', 0, 0, 53578.5, 0, 'null'),
(125, 'sky', 0, 0, 11.56, 0, '03 Jul 2021 9:40 PM', 'ksajib', 'club commission', 11.56, 0, 40291.3, 0, 'null'),
(126, 'sky', 0, 0, 578, 0, '03 Jul 2021 9:40 PM', '0', 'Bet Placed', 0, 0, 50000.5, 0, 'null'),
(127, 'sky', 0, 0, 2, 0, '05 Jul 2021 3:01 AM', 'ksajib', 'club commission', 2, 0, 40293.3, 0, 'null'),
(128, 'sky', 0, 0, 100, 0, '05 Jul 2021 3:01 AM', '0', 'Bet Placed', 0, 0, 49900.5, 0, 'null'),
(129, 'sky', 0, 0, 20, 0, '06 Jul 2021 10:44 PM', 'ksajib', 'club commission', 20, 0, 40313.3, 0, 'null'),
(130, 'sky', 0, 0, 1000, 0, '06 Jul 2021 10:44 PM', '0', 'Bet Placed', 0, 0, 3321, 0, 'null'),
(131, 'sky', 0, 0, 0, 1810, '06 Jul 2021 10:45 PM', '0', 'Bet Win', 0, 0, 5131, 0, 'null'),
(132, 'sky', 0, 0, 1300, 0, '07 Jul 2021 1:29 AM', '0', 'withdraw', 0, 0, 3831, 0, 'null'),
(133, 'sky', 0, 0, 1300, 0, '07 Jul 2021 1:30 AM', '0', 'withdraw', 0, 0, 2531, 0, 'null'),
(134, 'sky', 0, 0, 0, 1300, '07 Jul 2021 1:30 AM', '0', 'withrawal cancel', 0, 0, 3831, 0, 'null'),
(135, 'sky', 0, 0, 1000, 0, '07 Jul 2021 1:47 AM', '0', 'withdraw', 0, 0, 2831, 0, 'null');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `sponsorUsername` varchar(50) NOT NULL,
  `clubId` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `balance` double NOT NULL DEFAULT 0,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `loan` double NOT NULL DEFAULT 0,
  `sponsorCommission` double NOT NULL DEFAULT 0,
  `refresh` int(11) NOT NULL DEFAULT 1,
  `active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `userId`, `password`, `mobileNumber`, `sponsorUsername`, `clubId`, `email`, `balance`, `time`, `loan`, `sponsorCommission`, `refresh`, `active`) VALUES
(2, 'sky', 'sky', '123456', '01700000001', '', 'ksajib', 'sky@gmail.com', 2831, '2021-05-30 19:26:35', 0, 1, 0, 1),
(3, 'Mahide hasan', 'Mhasanbdfp', '01771994444', '01771994444', '', 'ksajib', 'mhasanbdfp@gmail.com', 49000, '2021-05-30 20:35:07', 0, 1, 1, 1),
(7, 'John Smith', 'smith', '123456', '01712345678', 'sky', 'ksajib', '', 0, '2021-07-12 18:04:20', 0, 0, 0, 1),
(8, 'david', 'david', '123456', '01732565663', 'sky', 'ksajib', '', 0, '2021-07-12 18:58:45', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userverifynumber`
--

CREATE TABLE `userverifynumber` (
  `id` int(11) NOT NULL,
  `userId` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `time` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_notify`
--

CREATE TABLE `user_notify` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notify`
--

INSERT INTO `user_notify` (`id`, `userId`) VALUES
(1, 'ksajib');

-- --------------------------------------------------------

--
-- Table structure for table `w_method`
--

CREATE TABLE `w_method` (
  `method` varchar(20) NOT NULL,
  `rate` double NOT NULL DEFAULT 0,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_method`
--

INSERT INTO `w_method` (`method`, `rate`, `id`) VALUES
('Bkash', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admintransaction`
--
ALTER TABLE `admintransaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`,`debit`,`credit`,`time`,`total`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `admin_notify`
--
ALTER TABLE `admin_notify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balance_transfer`
--
ALTER TABLE `balance_transfer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`userId`,`to_userId`,`amount`,`time`,`notes`,`userBalance`),
  ADD KEY `userId` (`userId`),
  ADD KEY `to_userId` (`to_userId`);

--
-- Indexes for table `bet`
--
ALTER TABLE `bet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `matchId` (`matchId`),
  ADD KEY `club` (`club`);

--
-- Indexes for table `betclosehistory`
--
ALTER TABLE `betclosehistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `matchId` (`matchId`),
  ADD KEY `queId` (`queId`);

--
-- Indexes for table `betting_subtitle`
--
ALTER TABLE `betting_subtitle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betting_sub_title_option`
--
ALTER TABLE `betting_sub_title_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betting_title`
--
ALTER TABLE `betting_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bettintransaction`
--
ALTER TABLE `bettintransaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bet_history`
--
ALTER TABLE `bet_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `admin` (`admin`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `id_2` (`id`,`name`,`userId`,`password`,`mobileNumber`,`email`,`balance`,`rate`,`active`,`personalIdOfClub`);

--
-- Indexes for table `default_ans`
--
ALTER TABLE `default_ans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_match`
--
ALTER TABLE `default_match`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_ques`
--
ALTER TABLE `default_ques`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_and_withdraw_his`
--
ALTER TABLE `deposit_and_withdraw_his`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`m_number`,`amount`,`startDate`,`finishDate`,`d_or_w`);

--
-- Indexes for table `ending_time`
--
ALTER TABLE `ending_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `global_setting`
--
ALTER TABLE `global_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hiddenmatch`
--
ALTER TABLE `hiddenmatch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `matchId` (`matchId`),
  ADD KEY `adminId` (`adminId`);

--
-- Indexes for table `ip`
--
ALTER TABLE `ip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iptrack`
--
ALTER TABLE `iptrack`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`ip_address`(255),`time`);

--
-- Indexes for table `ip_permit`
--
ALTER TABLE `ip_permit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `method`
--
ALTER TABLE `method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multibet`
--
ALTER TABLE `multibet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multibethelper`
--
ALTER TABLE `multibethelper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receiving_money_number`
--
ALTER TABLE `receiving_money_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresh`
--
ALTER TABLE `refresh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sending_money_number`
--
ALTER TABLE `sending_money_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `clubId` (`clubId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `clubId` (`clubId`),
  ADD KEY `active` (`active`),
  ADD KEY `refresh` (`refresh`),
  ADD KEY `balance` (`balance`);

--
-- Indexes for table `userverifynumber`
--
ALTER TABLE `userverifynumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notify`
--
ALTER TABLE `user_notify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`userId`);

--
-- Indexes for table `w_method`
--
ALTER TABLE `w_method`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `admintransaction`
--
ALTER TABLE `admintransaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `admin_notify`
--
ALTER TABLE `admin_notify`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `balance_transfer`
--
ALTER TABLE `balance_transfer`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bet`
--
ALTER TABLE `bet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `betclosehistory`
--
ALTER TABLE `betclosehistory`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `betting_subtitle`
--
ALTER TABLE `betting_subtitle`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `betting_sub_title_option`
--
ALTER TABLE `betting_sub_title_option`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `betting_title`
--
ALTER TABLE `betting_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `bettintransaction`
--
ALTER TABLE `bettintransaction`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bet_history`
--
ALTER TABLE `bet_history`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `club`
--
ALTER TABLE `club`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `default_ans`
--
ALTER TABLE `default_ans`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `default_match`
--
ALTER TABLE `default_match`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `default_ques`
--
ALTER TABLE `default_ques`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit_and_withdraw_his`
--
ALTER TABLE `deposit_and_withdraw_his`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ending_time`
--
ALTER TABLE `ending_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `global_setting`
--
ALTER TABLE `global_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hiddenmatch`
--
ALTER TABLE `hiddenmatch`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ip`
--
ALTER TABLE `ip`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `iptrack`
--
ALTER TABLE `iptrack`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_permit`
--
ALTER TABLE `ip_permit`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `method`
--
ALTER TABLE `method`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `multibet`
--
ALTER TABLE `multibet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `multibethelper`
--
ALTER TABLE `multibethelper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `receiving_money_number`
--
ALTER TABLE `receiving_money_number`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `refresh`
--
ALTER TABLE `refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rule`
--
ALTER TABLE `rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sending_money_number`
--
ALTER TABLE `sending_money_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `userverifynumber`
--
ALTER TABLE `userverifynumber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notify`
--
ALTER TABLE `user_notify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `w_method`
--
ALTER TABLE `w_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
