-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2018 at 06:38 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `betting`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `userName`, `password`) VALUES
(1, 'arif', 'arif@gmail.com', 'arif123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `deposit` double NOT NULL,
  `withdraw` double NOT NULL,
  `notificationType` int(11) NOT NULL,
  `pay_method` varchar(20) NOT NULL,
  `acc_type` varchar(20) NOT NULL,
  `from_number` varchar(20) NOT NULL,
  `to_number` varchar(20) NOT NULL,
  `ref_number` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_notification`
--

INSERT INTO `admin_notification` (`id`, `userId`, `deposit`, `withdraw`, `notificationType`, `pay_method`, `acc_type`, `from_number`, `to_number`, `ref_number`, `time`, `seen`) VALUES
(9, 'bet123', 89, 0, 1, 'Bkash', '', '78', '95769456', '78', '2018-09-22 22:01:21', 1),
(10, 'arif', 32, 0, 1, '', '', '2442', '24', '242', '2018-09-22 22:20:25', 1),
(11, 'bet123', 939, 0, 1, 'Bkash', '', '4242', '95769456', '2222', '2018-09-22 22:27:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bet`
--

CREATE TABLE `bet` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `club` varchar(50) NOT NULL,
  `betTitle` varchar(100) NOT NULL,
  `betTitleId` int(11) NOT NULL,
  `betId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `matchTitle` varchar(50) NOT NULL,
  `betRate` double NOT NULL,
  `betAmount` int(11) NOT NULL,
  `betStatus` int(11) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bet`
--

INSERT INTO `bet` (`id`, `userId`, `club`, `betTitle`, `betTitleId`, `betId`, `matchId`, `matchTitle`, `betRate`, `betAmount`, `betStatus`, `time`) VALUES
(35, 'bet123', 'bet123', 'Western Australia', 39, 35, 55, 'Full Time', 2.3, 50, 0, '2018-09-22 14:33:32'),
(36, 'bet123', 'bet123', 'jjj', 39, 38, 55, 'Full Time', 7, 100, 0, '2018-09-22 14:35:22'),
(37, 'bet123', 'bet123', 'hfhfh', 40, 36, 55, 'jhh', 8, 100, 0, '2018-09-22 14:35:42'),
(38, 'bet123', 'bet123', 'hfhfh', 40, 36, 55, 'jhh', 8, 100, 0, '2018-09-22 20:41:17'),
(39, 'bet123', 'bet123', 'hfhfh', 40, 36, 55, 'jhh', 8, 100, 0, '2018-09-22 20:41:25'),
(40, 'bet123', 'bet123', 'hfhfh', 40, 36, 55, 'jhh', 8, 10, 0, '2018-09-22 20:41:30'),
(41, 'bet123', 'bet123', 'hfhfh', 40, 36, 55, 'jhh', 8, 98, 0, '2018-09-22 20:41:47'),
(42, 'bet123', 'bet123', 'Western Australia', 39, 35, 55, 'Full Time', 2.3, 100, 0, '2018-09-22 20:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `betting_subtitle`
--

CREATE TABLE `betting_subtitle` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `bettingId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_subtitle`
--

INSERT INTO `betting_subtitle` (`id`, `title`, `bettingId`) VALUES
(39, 'Full Time', '55'),
(40, 'jhh', '55'),
(41, 'kjkhkhhk', '55');

-- --------------------------------------------------------

--
-- Table structure for table `betting_sub_title_option`
--

CREATE TABLE `betting_sub_title_option` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `bettingSubTitleId` int(11) NOT NULL,
  `betCount` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_sub_title_option`
--

INSERT INTO `betting_sub_title_option` (`id`, `title`, `amount`, `bettingSubTitleId`, `betCount`) VALUES
(35, 'Western Australia', '2.3', 39, 0),
(36, 'hfhfh', '8', 40, 0),
(37, 'hh', '4', 39, 0),
(38, 'jjj', '7', 39, 0),
(39, 'jjj', '2', 41, 0);

-- --------------------------------------------------------

--
-- Table structure for table `betting_title`
--

CREATE TABLE `betting_title` (
  `id` int(11) NOT NULL,
  `A_team` varchar(50) NOT NULL,
  `B_team` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `date` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `gameType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `betting_title`
--

INSERT INTO `betting_title` (`id`, `A_team`, `B_team`, `title`, `date`, `status`, `gameType`) VALUES
(55, 'Western Australia', ' South Australia', 'Australia Domestic One-Day Cup ', '22 Sep 2018 @ 12:00 ', 1, 1),
(56, 'Western Australia', 'South Australia', 'Australia Domestic One-Day Cup', '22 Sep 2018 @ 12:10 ', 1, 2),
(57, 'Western Australia', 'South Australia', 'Australia Domestic One-Day Cup', '22 Sep 2018 @ 12:20 ', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `userId` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `userId`, `msg`, `time`, `admin`) VALUES
(24, 'bet123', 'kk', '2018-09-22 20:53:19', 0),
(25, 'bet123', 'TypographyTypographyTypographyTypographyTypographyTypography', '2018-09-22 20:53:28', 0),
(26, 'bet123', 'Typography Typography Typography Typography', '2018-09-22 20:53:46', 0),
(27, 'bet123', 'sj sjd         dsgjjjjjjjjjjjjj jsdddddddddddd dsjjj  jsssssssssssssssssss sdjsddsds', '2018-09-22 20:55:10', 0),
(28, 'bet123', 'ksd', '2018-09-22 20:57:12', 0),
(29, 'bet123', 'kl', '2018-09-22 20:58:16', 0),
(30, 'bet123', 'a', '2018-09-22 20:58:22', 0),
(31, 'bet123', 'fgf', '2018-09-22 20:58:36', 0),
(32, 'bet123', 'fsf', '2018-09-22 20:59:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`id`, `name`, `userId`, `password`, `mobileNumber`, `email`) VALUES
(3, 'blue', 'blue123', '123', '01711158342', 'niranjantsc@gmail.com'),
(4, 'jhhg', 'Arifuzzaman', '234', '01711158342', 'niranjantsc@gmail.com'),
(5, 'hhggh', 'gggh', 'db85e2590b6109813dafa101ceb2faeb', 'g', 'g');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `up` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`up`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `deposit` double NOT NULL,
  `withdrawal` double NOT NULL,
  `debit` double NOT NULL,
  `credit` double NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `userId`, `deposit`, `withdrawal`, `debit`, `credit`, `time`) VALUES
(1, 'ap', 20, 1000, 4.6, 4, '2018-09-21 20:52:45'),
(2, 'bet123', 89, 0, 0, 0, '2018-09-22 22:01:21'),
(3, 'bet123', 939, 0, 0, 0, '2018-09-22 22:27:06');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `sponsorUsername` varchar(50) NOT NULL,
  `clubId` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `userId`, `password`, `mobileNumber`, `sponsorUsername`, `clubId`, `email`, `balance`) VALUES
(66, 'bet', 'bet123', '123456', '017456789900', '', '3', 'ar@gmail.com', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bet`
--
ALTER TABLE `bet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betting_subtitle`
--
ALTER TABLE `betting_subtitle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betting_sub_title_option`
--
ALTER TABLE `betting_sub_title_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betting_title`
--
ALTER TABLE `betting_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bet`
--
ALTER TABLE `bet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `betting_subtitle`
--
ALTER TABLE `betting_subtitle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `betting_sub_title_option`
--
ALTER TABLE `betting_sub_title_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `betting_title`
--
ALTER TABLE `betting_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `club`
--
ALTER TABLE `club`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
