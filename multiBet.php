<?php
include 'lib/Database.php';
$db = new Database();
?>

<?php

$con = $db->link;

$userId = $_COOKIE['userId'];
$details = $_POST['details'];
$details = base64_encode($details);
$total = $_POST['total'];
$betAmount = $_POST['betAmount'];
$rate = $_POST['rate'];
$returnAmount = $betAmount * $rate;

$userQuery = "SELECT * FROM `user` where userId = '$userId'";
$userResult = mysqli_query($con, $userQuery);

while($row = mysqli_fetch_assoc($userResult)){
    $userBalance = $row['balance'];
}

if($userBalance > $betAmount){
    $newBalance = $userBalance - $betAmount;
    $balanceUpdateQuery = "UPDATE `user` SET `balance` = '$newBalance' WHERE `user`.`userId` = '$userId'";
    mysqli_query($con, $balanceUpdateQuery);
    
    // insert multibet details in database
    $insertQuery = "INSERT INTO `multiBet` (`id`, `betBy`, `details`, `rate`, `amount`, `returnAmount`, `won`, `gameLeft`, `status`) VALUES (NULL, '$userId', '$details', '$rate', '$betAmount', '$returnAmount', '0', '$total', '2')";
    // status 0 = lost, 1 = won, 2 = pending
    mysqli_query($con, $insertQuery);
    
    // get the last row id
    $getIdQuery = "SELECT * FROM `multiBet` order by id DESC LIMIT 1";
    $getIdResult = mysqli_query($con, $getIdQuery);
    while($row = mysqli_fetch_assoc($getIdResult)){
        $lastInsertedId = $row['id'];
    }
    
    
    
    // multiBet helper database insertion
    
    $details = base64_decode($details);
    $details = json_decode($details);
    // print_r($details);
    
    for($i=0; $i<$total; $i++){
        $betId = $details->details[$i]->betId;
        $questionId = $details->details[$i]->betTitleId;
        $checkQuery = "SELECT * FROM `multiBetHelper` where ansId = $betId";
        $checkResult = mysqli_query($con, $checkQuery);
        if(mysqli_num_rows($checkResult) == 0){
            // create new row for $betId
            $insertQuery = "INSERT INTO `multiBetHelper` (`id`, `ansId`, `questionId`, `total`, `whoBeted`, `status`) VALUES (NULL, '$betId', '$questionId', '1', '$lastInsertedId', '2')";
            mysqli_query($con, $insertQuery);
        } else {
            // already a row avilable, 
            // now update the row
            while($row = mysqli_fetch_assoc($checkResult)){
                $arrOld = $row['whoBeted'];
                $totalOld = $row['total'];
            }
            $newTotal = $totalOld + 1;
            $newArr = explode(',', $arrOld);
            array_push($newArr, $lastInsertedId);
            $newArr = implode(",", $newArr);
            
            $updateQuery = "UPDATE `multiBetHelper` SET `total` = '$newTotal', `whoBeted` = '$newArr', `status` = '2' WHERE `multiBetHelper`.`ansId` = $betId";
            mysqli_query($con, $updateQuery);
            
        }
    }
    
    echo "Successful!";
} else {
    echo "Balance Insufficient!";
}

// echo $userBalance.'  '.$betAmount.'  '.$rate;



?>