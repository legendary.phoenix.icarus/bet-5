<?php include './header.php'; ?>

<!-- javascript for multibet -->
<script>
var games = [];
var map = [];
var RATE = 0;
var index = 0;
function updateMB() {
  if (games != []) {
    document.getElementById("mb").style.display = "inline";
  } else {
    document.getElementById("mb").style.display = "none";
  }

  document.getElementById("mb_total").innerHTML = games.length;
  document.getElementById("mb_rate").innerHTML = RATE;
  // document.getElementById("mb_amount")
  document.getElementById("mb_return").innerHTML = parseFloat(
    document.getElementById("mb_amount").value * RATE
  );
}

function helper(val) {
  //   var return_ = document.getElementById("mb_return");
  //   var rate = parseFloat(document.getElementById("mb_rate").innerHTML);
  //   // console.log(rate, val);
  //   return_.innerHTML = parseFloat(rate * val);
}

function removeFromCart(i) {
    RATE -= games[i].betRate;
  var m = map.indexOf(games[i].betId);
  games.splice(i, 1);
  map.splice(m, 1);

  updateMB();
  showSelected();
}

function addToCart() {
  var found = false;

  var match = $("#match").val();
  var matchBet = $("#matchBet").val();
  var betId = $("#betId").val();
  var matchId = $("#matchId").val();
  var betTitleId = $("#betTitleId").val();
  var betRate = $("#betRate").val();

  if (
    match == "" ||
    matchBet == "" ||
    betId == "" ||
    matchId == "" ||
    betTitleId == "" ||
    betRate == ""
  ) {
    return;
  }

  var hid = "#headingOne" + matchId + " h4";
  var info = document.querySelector(hid).innerHTML.trim();

  var game = {
    matchInfo: info,
    match: match,
    matchBet: matchBet,
    betRate: betRate,
    betId: betId,
    matchId: matchId,
    betTitleId: betTitleId,
  };

  for (var i = 0; i < map.length; i++) {
    if (map[i] == matchId) {
      found = true;
      break;
    } else {
      found = false;
    }
  }

  if (found == true) {
    alert("Error! May be you have already added this game!");
    found = false;
  } else {
    games.push(game);
    map.push(matchId);
    RATE += parseFloat(betRate);

    document.getElementById("addBetLoad").style.display = "block";

    setTimeout(function () {
      document.getElementById("addBetLoad").style.display = "none";
    }, 1000);
  }

  updateMB();

    //  console.log(betId, matchId, betTitleId);
}

function showSelected() {
  //         <div class="col-sm-6">
  //     <div class="card">
  //       <div class="card-body">
  //         <h5 class="card-title">Special title treatment</h5>
  //         <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
  //         <a href="#" class="btn btn-primary">Go somewhere</a>
  //       </div>
  //     </div>
  //   </div>
  var showSelected_ = document.getElementById("showSelected_");
  showSelected_.innerHTML = "";
  for (var i = 0; i < games.length; i++) {
    var hid = "headingOne" + games[i].matchId;
    var q = document.getElementById(hid).innerHTML;

    // console.log(q, hid, games[i].matchId);

    showSelected_.innerHTML +=
      `        <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">` +
      q +
      `</h5>
        <p class="card-text">` +
      "Question: " +
      games[i].match +
      "<br> Your Answer: " +
      games[i].matchBet +
      `</p>
        <a onclick="removeFromCart(` +
      i +
      `)" class="btn btn-primary">Remove</a>
      </div>
    </div>
  </div>`;
  }
}

function placeTheMultiBet() {
  if (
    $("#mb_amount").val() == "" ||
    $("#mb_amount").val() < 20 ||
    $("#mb_amount").val() > 100
  ) {
    alert("Amount must be greater than 20tk and less than 100tk.");
    return;
  } else if (games == "") {
    alert("Please select at least 1 game for the multibet!");
    return;
  }
  document.getElementById("multiBetSubmit").innerHTML =
    "Submiting your request!...";
  document.getElementById("multiBetSubmit").disabled = true;
  var jsonDeatils = { details: games };
  $.ajax({
    method: "POST",
    url: "multiBet.php",
    data: {
      details: JSON.stringify(jsonDeatils),
      total: games.length,
      betAmount: $("#mb_amount").val(),
      rate: RATE,
    },
    success: function (data) {
      alert(data);
      location.reload();
    //   console.log(data);
    },
  });
}

</script>


<link rel="stylesheet" href="css/indexStyle.css">
 <link href="https://fonts.googleapis.com/css?family=Palanquin&display=swap" rel="stylesheet">
   <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">-->


<style>
body{
    font-family: arial;
}
        .tabBlock
        {
            background-color:#57574f;
            border:solid 0px #FFA54F;
            border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;
            max-width:200px;
            width:100%;
            overflow:hidden;
            display:block;
        }
        .clock
        {
            vertical-align:middle;
            font-family:Orbitron;
            font-size:40px;
            font-weight:normal;
            color:#FFF;
            padding:0 10px;
        }
        .clocklg 
        {
            vertical-align:middle; 
            font-family:Orbitron;
            font-size:20px;
            font-weight:normal;
            color:#FFF;
        }
    </style>
    
    
    
    
        <!-- Modal withdraw -->
        <div id="withdraw" class="modal fade" role="dialog" >
            <div class="modal-dialog  " >

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header m-head" style="  background: #017B5B !important;">

                        <button type="button" class="close" data-dismiss="modal" style="color: #ffffff">&times;</button>
                        <h4 class="modal-title" style="color: black"> &nbsp;Request a withdraw</h4>
                    </div>
                    <div class="modal-body" style="padding: 2% !important">
                        <div class="">
                            <div role="form" class="register-form">
<strong> Minimum Withdraw 500 TK !!</strong>


<div id="errorWithraw" class="alert alert-danger errorWithraw" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        ×</button>  <strong>  Opps !!</strong> <span id="errorWithrawText"></span>
                                </div>


                                <hr class="colorgraph">
                                <div class="row">
                  

                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">

                                        <div class="form-group">
                                            <label style="text-align: left;width: 100%;padding-left:2px;">Amount <span style="color:#DD4F43;">*</span></label>
                                            <input type="text" name="first_name" id="wAmount-c" class="form-control input-lg" placeholder="Amount" tabindex="1">
                                        </div>
                                    </div>
                                    <!---->
                                    <div class="col-xs-12 col-sm-6 col-md-6">

                                        <div class="form-group">
                                            <label style="text-align: left;width: 100%;">Number <span style="color:#DD4F43;">*</span></label>
                                            <input type="text" name="number_" id="number_c_" class="form-control input-lg" placeholder="Number" tabindex="1">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <label style="text-align: left;width: 100%;">Password <span style="color:#DD4F43;">*</span></label>
                                        <div class=
                                        "form-group">
                                            <input type="text" name="password" id="password-c" class="form-control input-lg" placeholder="Password" tabindex="1">
                                        </div>
                                    </div>

                                </div>

                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6"><input type="submit" id="withdrawSubmit-c" value="Submit" class="btn btn-success btn-block btn-lg" tabindex="7"style="background: #017B5B"></div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        
    
    
<section class="callaction " style="border-bottom: 1px solid #3D7EA6;min-height: 450px;">

    <div class="content-container mx-auto p-0">

        <div class="" >



            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12  pl-0 pr-0 ">
                <div >
                    <div >

                        <div class="">
                            <div class="list-group" >
                                <?php
                                $query = "SELECT * FROM `notice`";
                                $result = $db->select($query);
                                $notice = $result->fetch_assoc();
                                ?>
                                <marquee style="color: #C4C4C4;background: #203F61;background-image: linear-gradient(to right top, #ffffff, #f5fcff, #e2fbff, #d1fbf9, #cef9e2);color:#795BDF" class="mrq" scrollamount='3' direction="scroll"> <?php echo $notice['text']; ?></marquee>
                                



                             	<!-- main -->
<section id="main" class="clearfix home-default pt-xs">
    <div class="container">

        
        <div class="row nsection slides">
            <div class="col-md-12">
                <!-- Jssor Slider Begin -->
                <script type="text/javascript" src="https://www.dolarbikroy.com/assets/slider/js/jssor.slider-27.5.0.min.js"></script>

                
                    <!-- #region Jssor Slider Begin -->
                    <script type="text/javascript"
                            src="https://www.dolarbikroy.com/assets/slider/js/jssor.slider-27.5.0.min.js"></script>


                    <script type="text/javascript">
                        jssor_1_slider_init = function () {

                            var jssor_1_SlideshowTransitions = [
                                {
                                    $Duration: 1500,
                                    x: 0.2,
                                    y: -0.1,
                                    $Delay: 80,
                                    $Cols: 10,
                                    $Rows: 5,
                                    $Opacity: 2,
                                    $Clip: 15,
                                    $During: {$Left: [0.2, 0.8], $Top: [0.2, 0.8]},
                                    $SlideOut: true,
                                    $Easing: {$Left: $Jease$.$InWave, $Top: $Jease$.$InWave, $Clip: $Jease$.$Linear},
                                    $Round: {$Left: 0.8, $Top: 2.5}
                                },
                                {
                                    $Duration: 1500,
                                    x: -1,
                                    y: 0.5,
                                    $Delay: 800,
                                    $Cols: 10,
                                    $Rows: 5,
                                    $Opacity: 2,
                                    $SlideOut: true,
                                    $Reverse: true,
                                    $Formation: $JssorSlideshowFormations$.$FormationRectangle,
                                    $Assembly: 260,
                                    $Easing: {$Left: $Jease$.$Linear, $Top: $Jease$.$OutJump},
                                    $Round: {$Top: 1.5}
                                },
                                {$Duration: 1000, y: 1, $Opacity: 2, $Easing: $Jease$.$InBounce},
                                {
                                    $Duration: 4000,
                                    x: -1,
                                    y: 0.45,
                                    $Delay: 80,
                                    $Cols: 12,
                                    $Opacity: 2,
                                    $Clip: 15,
                                    $During: {$Left: [0.35, 0.65], $Top: [0.35, 0.65], $Clip: [0, 0.15]},
                                    $SlideOut: true,
                                    $Formation: $JssorSlideshowFormations$.$FormationStraight,
                                    $Assembly: 2049,
                                    $Easing: {$Left: $Jease$.$Linear, $Top: $Jease$.$OutWave, $Clip: $Jease$.$OutQuad},
                                    $ScaleClip: 0.7,
                                    $Round: {$Top: 4}
                                },
                                {
                                    $Duration: 600,
                                    x: -1,
                                    y: 1,
                                    $Delay: 50,
                                    $Cols: 10,
                                    $Rows: 5,
                                    $Opacity: 2,
                                    $Formation: $JssorSlideshowFormations$.$FormationSwirl,
                                    $Assembly: 264,
                                    $Easing: {$Top: $Jease$.$InQuart, $Opacity: $Jease$.$Linear}
                                },
                                {
                                    $Duration: 1600,
                                    y: -1,
                                    $Delay: 40,
                                    $Cols: 24,
                                    $SlideOut: true,
                                    $Formation: $JssorSlideshowFormations$.$FormationStraight,
                                    $Easing: $Jease$.$OutJump,
                                    $Round: {$Top: 1.5}
                                },
                                {
                                    $Duration: 1000,
                                    x: 4,
                                    y: 4,
                                    $Zoom: 11,
                                    $SlideOut: true,
                                    $Easing: {
                                        $Left: $Jease$.$InExpo,
                                        $Top: $Jease$.$InExpo,
                                        $Zoom: $Jease$.$InExpo,
                                        $Opacity: $Jease$.$Linear
                                    },
                                    $Opacity: 2
                                },
                                {
                                    $Duration: 1000,
                                    $Zoom: 1,
                                    $Rotate: 1,
                                    $SlideOut: true,
                                    $Easing: {$Opacity: $Jease$.$Linear},
                                    $Opacity: 2,
                                    $Round: {$Rotate: 0.5}
                                },
                                {
                                    $Duration: 800,
                                    $Zoom: 11,
                                    $Easing: {$Zoom: $Jease$.$InOutExpo},
                                    $Opacity: 2,
                                    $Brother: {
                                        $Duration: 600,
                                        $Zoom: 11,
                                        $Easing: {$Zoom: $Jease$.$InOutExpo},
                                        $Opacity: 2,
                                        $Shift: -100
                                    }
                                },
                                {
                                    $Duration: 800,
                                    $Zoom: 1,
                                    $Rotate: 0.5,
                                    $Easing: {$Rotate: $Jease$.$InSine, $Zoom: $Jease$.$Swing},
                                    $Opacity: 2,
                                    $Brother: {
                                        $Duration: 800,
                                        $Zoom: 11,
                                        $Rotate: -0.5,
                                        $Easing: {$Rotate: $Jease$.$InSine, $Zoom: $Jease$.$Swing},
                                        $Opacity: 2,
                                        $Shift: 200
                                    }
                                },
                                {
                                    $Duration: 800,
                                    x: 0.3,
                                    y: 0.3,
                                    $Cols: 2,
                                    $Rows: 2,
                                    $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                                    $ChessMode: {$Column: 3, $Row: 12},
                                    $Easing: {
                                        $Left: $Jease$.$InCubic,
                                        $Top: $Jease$.$InCubic,
                                        $Opacity: $Jease$.$Linear
                                    },
                                    $Opacity: 2
                                }
                            ];

                            var jssor_1_options = {
                                $AutoPlay: 1,
                                $SlideshowOptions: {
                                    $Class: $JssorSlideshowRunner$,
                                    $Transitions: jssor_1_SlideshowTransitions
                                },
                                $ArrowNavigatorOptions: {
                                    $Class: $JssorArrowNavigator$
                                },
                                $BulletNavigatorOptions: {
                                    $Class: $JssorBulletNavigator$
                                }
                            };

                            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                            /*#region responsive code begin*/

                            var MAX_WIDTH = 3000;

                            function ScaleSlider() {
                                var containerElement = jssor_1_slider.$Elmt.parentNode;
                                var containerWidth = containerElement.clientWidth - 30;

                                if (containerWidth) {

                                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                                    jssor_1_slider.$ScaleWidth(expectedWidth);
                                } else {
                                    window.setTimeout(ScaleSlider, 30);
                                }
                            }

                            ScaleSlider();

                            $Jssor$.$AddEvent(window, "load", ScaleSlider);
                            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                            /*#endregion responsive code end*/
                        };
                    </script>


                    <style>
                        /*jssor slider loading skin spin css*/
                        .jssorl-009-spin img {
                            animation-name: jssorl-009-spin;
                            animation-duration: 1.6s;
                            animation-iteration-count: infinite;
                            animation-timing-function: linear;
                        }

                        @keyframes jssorl-009-spin {
                            from {
                                transform: rotate(0deg);
                            }
                            to {
                                transform: rotate(360deg);
                            }
                        }

                        /*jssor slider bullet skin 051 css*/
                        .jssorb051 .i {
                            position: absolute;
                            cursor: pointer;
                        }

                        .jssorb051 .i .b {
                            fill: #fff;
                            fill-opacity: 0.5;
                        }

                        .jssorb051 .i:hover .b {
                            fill-opacity: .7;
                        }

                        .jssorb051 .iav .b {
                            fill-opacity: 1;
                        }

                        .jssorb051 .i.idn {
                            opacity: .3;
                        }

                        /*jssor slider arrow skin 051 css*/
                        .jssora051 {
                            display: block;
                            position: absolute;
                            cursor: pointer;
                        }

                        .jssora051 .a {
                            fill: none;
                            stroke: #fff;
                            stroke-width: 360;
                            stroke-miterlimit: 10;
                        }

                        .jssora051:hover {
                            opacity: .8;
                        }

                        .jssora051.jssora051dn {
                            opacity: .5;
                        }

                        .jssora051.jssora051ds {
                            opacity: .3;
                            pointer-events: none;
                        }
                    </style>


                    <div id="jssor_1"
                         style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
                        <!-- Loading Screen -->
                        <div data-u="loading" class="jssorl-009-spin"
                             style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                                 src="https://www.dolarbikroy.com/assets/slider/svg/loading/static-svg/spin.svg"/>
                        </div>

                        <div data-u="slides"
                             style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">

                            <div>
                                <img data-u="image"
                                     src="1.jpg"/>
                            </div>

                            <div>
                                <img data-u="image"
                                     src="2.jpg"/>
                            </div>

                            <div>
                                <img data-u="image"
                                     src="3.jpg"/>
                            </div>

                            <div>
                                <img data-u="image"
                                     src="4.jpg"/>
                            </div>

                        </div>

                        
<!-- Bullet Navigator -->
                        <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;"
                             data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                                <svg viewbox="0 0 16000 16000"
                                     style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                                </svg>
                            </div>
                        </div>

                        <!-- Arrow Navigator -->
                        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;"
                             data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                            <svg viewbox="0 0 16000 16000"
                                 style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                            </svg>
                        </div>

                        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;"
                             data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                            <svg viewbox="0 0 16000 16000"
                                 style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                            </svg>
                        </div>

                    </div>
                    <script type="text/javascript">jssor_1_slider_init();</script>
                    <!-- #endregion Jssor Slider End -->

                                <!-- Jssor Slider End -->
            </div>

        </div>

                                    <div class="tab-content ">
                                        <div class="tab-pane active" id="1">

                                        </div>
                                        <div class="tab-pane" id="2">

                                        </div>
                                        <div class="tab-pane" id="3">

                                        </div>
                                   
                                    </div>
                                </div>


                                <a style="background: #DBF6E9 ; color:#3D7EA6;"  class="list-group-item"><span class=""> <i class="fa-spin"><img src="img/live.gif" /></i> Live Match</span><span class="glyphicon glyphicon-menu-right mg-icon pull-right"></span></a>
                                <div id="dataModal" class="modal fade">  
                                    <div class="modal-dialog">  
                                        <div class="modal-content">  
                                            <div class="modal-header">  
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                                <h4 class="modal-title">Employee Details</h4>  
                                            </div>  
                                            <div class="modal-body" id="employee_detail">  
                                            </div>  
                                            <div class="modal-footer">  
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                                            </div>  
                                        </div>  
                                    </div>  
                                </div>  
                                <!-- Modal betting-->

                                <div   class="modal fade betForm" id="betting" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content m-content">
                                            <div class="modal-header m-head" style="  background: #3D7EA6 !important;">

                                                <button type="button" class="close" data-dismiss="modal" style="color: #ffffff">&times;</button>
                                                <h4 class="modal-title" style="color: #D2D2D2">  &nbsp; Place Bet</h4>
                                            </div>
                                            <div class="modal-body" style="padding: 2% !important">
                                                <div class="signup-form">

                                                    <div  id="formData">

                                                        <div id="errorPlaceBet" class="alert alert-danger errorPlaceBet" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                                ×</button>  <strong>  Opps !!</strong> <span id="PlaceBetText"></span>
                                                        </div>






                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-7">
                                                                    <label><span id="BettingSubTitleOption"></span> <span id="betRateShow" class="badge text-right"></span></label> <br>
                                                                    <label><small id="bettingSubTitle"></small></label><br>
                                                                    <label>
                                                                        <span id="bettingTitle"></span>
                                                                        <span  id="gameLiveOrUpcoming"  class="badge text-right"></span>

                                                                    </label><br>
                                                                    <input type="text"  id="match" value="" hidden="1">
                                                                    <input type="text"  id="matchBet"value="" hidden="1">
                                                                    <input type="text"  id="betRate"value="" hidden="1">
                                                                    <input type="text"  id="betId"value="" hidden="1">
                                                                    <input type="text"  id="matchId" value="" hidden="1">
                                                                    <input type="text"  id="betTitleId" value="" hidden="1">

                                                                    <label class="gameLogo">

                                                                        <img id="gameLogo" style="box-shadow: 1px 7px 4px 0px #787981 !important;border-radius: 50%;" class="img-circle" src="" width="25px;">&nbsp;


                                                                    </label>

                                                                </div>
                                                                <div class="col-lg-5">

                                                                    <input type="text" class="form-control"   id="stakeAmount" value="" >


                                                                </div>

                                                            </div>



                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-7">
                                                                    <label><strong>Total Stake</strong></label>

                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <label class="col-lg-12 text-right"><strong id="stakeAmountView">100</strong></label>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-7">
                                                                    <label><strong>Possible Winning</strong></label>

                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <label class="col-lg-12 text-right"><strong id="possibleAmount"> 100.00</strong></label>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">

                                                            <button  type="submit" id="placeBet"  name="placeBet"  class="btn btn-info btn-lg btn-block" style="background: #3D7EA6;">Place Bet</button>
                                                       <!-- <center><b>Or,</b></center>
                                                            <br>
                                                            
                                                            <button class="btn btn-info btn-lg btn-block" style="background: #3D7EA6;" id="addBet" onclick="addToCart()">Select for multibet</button> -->

                                                            <button  id="load" class="btn btn-success btn-sm btn-block load"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></button>
                                                        </div>
                                                        
                                                        <button  id="addBetLoad" class="btn btn-success btn-sm btn-block load"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                               
                               
                                        <!-- Button trigger modal -->
    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">-->
    <!--  Launch demo modal-->
    <!--</button>-->
    
    <!-- Modal -->
    <div class="modal fade" id="viewSelected" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Selected for the multibet</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <!-- body start -->
<div class="row" id="showSelected_">
  
</div>
<!--body end-->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
                                        
                                        
                                        
                                        |
                                        
                                        <strong>Rate: <span id="mb_rate">2</span>
                                        <input type="number" style="display: none" id="mb_rate_input" value="2"></strong> |
                                        <strong>Bet: 
                                            <!--<input onkeyup="helper(this.value)" style="width: 70px" type="number" id="mb_amount">-->
                                            <input onkeyup="updateMB()" onkeydown="updateMB()" style="width: 70px" type="number" id="mb_amount">
                                        Tk</strong> |
                                        <strong>Return: <span id="mb_return">0</span> Tk</strong>
                                        <button id="multiBetSubmit" onclick="placeTheMultiBet()" style="float: right" type="button" class="btn btn-primary">place the multibet</button>

                                    </div> 
                                </div>
                                
                                <div id="liveContent">


                           
                                </div>

                                <a style=" background: #3D7EA6;color:#13ffca";"  class="list-group-item"><span class=""><i class="fa-spin"><img src="img/live.gif" /></i> Upcoming Match</span><span class="glyphicon glyphicon-menu-right mg-icon pull-right"></span></a>
                                <div id="upcomingContent">
                               
                                </div>


                            </div><!-- ./ end list-group -->
                        </div><!-- ./ end slide-container -->

                    </div><!-- ./ end panel-body -->
                </div><!-- ./ end panel panel-default-->
            </div><!-- ./ endcol-lg-6 col-lg-offset-3 -->
            <div class="col-lg-2">

            </div>
        </div><!-- ./ end row -->

    </div>
</section>
<footer class="footer-basic-centered ">
    <div class="container">


        <div class="row">

            <div class="col-lg-6">
                  <p class="footer-links">

                    <a href="index.php">Home</a>
                    |
                    <a href="#"> Contact-us</a>
                    |
                    <a href="#"> Rules & Regulations</a>
                    |
                    <a href="#"> FAQ</a>
                    |
                    <a href="#"> About Us</a>

                </p>
            </div>
            <div class="col-lg-3">
   
                               <p id="date_clock"  style="color:red">

</p>
                                 <script>
var d = new Date();
document.getElementById("date_clock").innerHTML =  d.toDateString();
</script>
                            <p id="MyClockDisplay" onload="showTime()"></p>

                            
                            <script type="text/javascript">
                                function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " ";
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();
                            </script>
                            
                           <br/> 
    <a  class="" href=""><img style="width: 150px;height: 60px;margin-left: 10px;" src="img/logo.png"></a><br/>
                <p style="font-size: 15px;color: #dcdcdc;" class="footer-company-name"> LiveOn77.Com &copy; <?php echo date("Y"); ?>  all right reserved.</p>
            </div>
            <div class="col-lg-3">
                <span class="userAlert"> Caution! We are strongly discourage to use this site who are not 18+ and also site administrator is not liable to any kind of issues created by user.</span>
            </div>

        </div>
    </div>

</footer>
</div>
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min_1.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/validation/placeBet.js"></script>

<script src="js/validation/validated.js"></script>
<script src="js/validation/siteRefresh.js"></script>
<script src="js/validation/deposit_and_withdraw.js"></script>


<script>
$(document).ready(function () {
     $("#liveContent").load('content.php');
     $("#upcomingContent").load('upcomingMatch.php');

}); 
</script>
<?php
if (isset($_COOKIE["userId"]) AND ( isset($_COOKIE["password"]))) {

    include './chatBox.php';
}
?>


<style type="text/css">
    .dateshow {
    text-align: center;
    padding: 4px 0px;
    color: #C4C4C4;
    background:#022644;
    border-left: 2px solid #D8AB7D;
    border-right: 2px solid #D8AB7D;
    font-size: 15px;
    border-bottom: 1px solid #9E7143;
    border-left: 2px solid #D8AB7D;
    border-right: 2px solid #D8AB7D;
    font-size: 15px;
    border-bottom: 1px solid #9E7143;
    margin-top: -3px;
    margin-bottom: 4px;
    font-style: arial;
    font-weight: bold;
}
</style>